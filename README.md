### Overview ###
This project is a project that creates a contacts book where the user is able to keep a record of their contacts details (phone number, etc)
 
### Languages and Libraries ###
Java: The entirety of the code is written in Java. This decision was made by the team as it was a language that everyone was proficient in and had several libraries that supported UI development. Java’s memory management also has built in garbage collection which helps simplify the coding needed to complete the application
Swing: The Java Swing library provides lightweight components designed to work the same across platforms. For this reason, it was decided that the majority of the UI would rely on Swing. The classes that were used were JButton, JLabel, JFrame, JTextPane, JTextField JScrollPane and JTable. Major Backend architectural descisions were made based on the decision to use JTable. The fact that the JTable class stores its information in two-dimensional object arrays was used when designing the Book class. This is described further later in the document.

### Overall Design ###
The overall application can be split into two components, the backend and frontend. There are two backend classes, Sorting and Book. The frontend primarily consists of two classes a ContactsBookManager and ContactsBook class. The relationship between these two classes is that ContactsBookManager consists of an ArrayList of ContactsBook. Everytime a new contact book tab is created a new instant of ContactsBook is added to ContactsBookManager's ArrayList. ContactsBook contains an instant of the Book class from the backend which contains storage and manipulation capabilities for contacts. The Sorting class is also used in the ContactsBook class whenever sorting is requested by the user. 

---------------------------Backend-----------------

### Sorting Class ###
Per project specifications contacts must be sorted by last name and zip code. This was accomplished by having a ‘Sorting’ class that was responsible for sorting contacts within a book.  This class has nested classes (SortByLastName, SortByFirstName, SortByZipCode, SortByEmail, SortByPhone, SortByAddressLine1, SortByAddressLine2, SortByCity, SortByState) that implement the comparator class. In each class the compare method is overridden to define the specific method of sorting. The “sortContacts” method contains a switch statement that sorts the contacts in a Book based on a particular enum.  A single Boolean attribute, sortUp_, exists which will be set to true when sorting is requested by the ContactsBookHeaderEventHandler class during a category mouse click handler in the frontend. sortUp_  toggles between true and false each time a user clicks a category header and allows sorting to be done alphabetically A-Z or Z-A.

### Book Class ###
The book class is the main class that is used in the backend to save contact information within a particular contact book. 
Attributes
_bookName:
String
Used for user to differentiate between various contact books
_contactsBook:
ArrayList<Object[]>
Each sub Object[] contains information about a particular contact
This particular type was choosen because it is compatible with the JTable library and ArrayLists dynamically resize
_searchedContactsBooks:
ArrayList<Object[]>
Contains contacts to be displayed that match the search criteria
_bookFileNameAndPath:
String
specifies a location to allow saving the address book's contacts (_contactsBook)
_bookExportNameAndPath:
String
specifies a name and a location path for a file that needs to be exported or imported


------------------ FontEnd --------------------------------------

### ContactsBook ###

The following diagram shows the various Swing and backend components that were used to create the basic functionality of a single address book. This class is used in ContactsBookManager which stores multiple address books. 
Book: ContactsBook has an instant of the Book class which is used to store information about mutliple contacts. This is where the frontend stores the information about each contact.
JFrame: An instance of JFrame, popup, is created and enabled when a new contact is being added. When a error is raised, the renderErrorMessageModal() will produce a JFrame instance that contains a error message.
JTable: There is one JTable, table, that is used in a instance of ContactsBook. A closely associated function is the loadJTableWithData() which takes a two dimensional object array (an attribute in the Book class) and loads the table with the contacts.
JButton: There are several JButtons in the interface. These include but are not limited to: delete_contact_button, create_button, edit_button, save_button, cancel_button. Each of these buttons have an unique event handler which performs the appropriate action when the button is pressed.  These event handler classes either extend MouseAdpater or implement ActionListener.
JTextPane: There are several JTextPane in this interface, some editable others not. Examples of these are the category headers in the UI, editable fields in a new contact popup, and the uneditable fields in the display contact popup. 


### ContactsBookManager ###

ContactsBookManager properties:
ArrayList of ContactsBook: The ContactsBookManager contains an arraylist of ContactsBook allowing the user to create multiple contact books. 
JMenu/JMenuItem: These items are responsible for the menu item in the top left of the application. The JMenu item "File" contains JMenuItems, "New", "Open", "Save", "Save As", "Close", and exit "Exit". The other JMenu item "Tools" contain the JMenuItems, "Export" and "Import". This structure is created in initializeMenuBar(). The functionality of the JMenuItems are implemented in event handler classes that implement the ActionListener class.
JButton:  The "Add Contact" JButton is the only JButton that is used in ContactsBookManager. This allows the user to create a new contact in the ContactsBook they are currently in. The functionality of this button is handled in the AddContactEventHandler class
JFrame: There is one main JFrame, frame, which consists of all the other components
JTextField: There are two important JTextFieldComponents in this class. The first one is essentially the search bar present at the top left corner of the application. The searching functionality is found in the Book class. The second is the current book title field, book_title_field.
