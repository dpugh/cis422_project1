package wusers;

import java.util.*;

/*****************************************************************************************************
 This is a class that is responsible for all of the sorting for our contacts book.  The class has
 innerclasses which define how the arrays will be sorted. This classes method 'sortContacts' uses
 these inner classes to perform the actual sorting.
 *****************************************************************************************************/
@SuppressWarnings("Duplicates")
class Sorting {

    private Boolean _sortUp;
    /*****************************************************************************************************
        This is a class that is responsible for sorting an arraylist of contacts by their last name.
        If there is a tie between the last names then there will be a tie break by the contacts first names.
     *****************************************************************************************************/
    private class SortByLastName implements Comparator<Object[]>{

        @Override
        public int compare(Object[] object1, Object[] object2) {
            String lastNameObject1 = (String) object1[ContactAttributeIndexer.LASTNAME.getIndex()];
            String lastNameObject2 = (String) object2[ContactAttributeIndexer.LASTNAME.getIndex()];
            String firstNameObject1 = (String) object1[ContactAttributeIndexer.FIRSTNAME.getIndex()];
            String firstNameObject2 = (String) object2[ContactAttributeIndexer.FIRSTNAME.getIndex()];
            if(lastNameObject1.equals("") && lastNameObject2.equals("")){
                return 0;
            }
            if(lastNameObject1.equals("")){
                return 1;
            }
            if(lastNameObject2.equals("")){
                return -1;
            }
            if(!_sortUp) {
                lastNameObject1 = (String) object2[ContactAttributeIndexer.LASTNAME.getIndex()];
                lastNameObject2 = (String) object1[ContactAttributeIndexer.LASTNAME.getIndex()];
                firstNameObject1 = (String) object2[ContactAttributeIndexer.FIRSTNAME.getIndex()];
                firstNameObject2 = (String) object1[ContactAttributeIndexer.FIRSTNAME.getIndex()];
            }
            if(lastNameObject1.equals(lastNameObject2)){
                if(firstNameObject1.equals("") && firstNameObject2.equals("")){
                    return 0;
                }
                if(firstNameObject1.equals("")){
                    return 1;
                }
                if(firstNameObject2.equals("")){
                    return -1;
                }
                return firstNameObject1.compareToIgnoreCase(firstNameObject2);
            }
            else{
                if(lastNameObject1.equals("")){
                    return 1;
                }
                return lastNameObject1.compareToIgnoreCase(lastNameObject2);
            }
        }
    }

    /*****************************************************************************************************
     This is a class that is responsible for sorting an arraylist of contacts by their first name.
     *****************************************************************************************************/
    private class SortByFirstName implements Comparator<Object[]>{

        @Override
        public int compare(Object[] object1, Object[] object2) {

            String firstNameObject1 = (String) object1[ContactAttributeIndexer.FIRSTNAME.getIndex()];
            String firstNameObject2 = (String) object2[ContactAttributeIndexer.FIRSTNAME.getIndex()];

            if(firstNameObject1.equals("") && firstNameObject2.equals("")){
                return 0;
            }
            if(firstNameObject1.equals("")){
                return 1;
            }
            if(firstNameObject2.equals("")){
                return -1;
            }
            if(_sortUp){
                if(firstNameObject1.equals("")){
                    return 1;
                }
                return firstNameObject1.compareToIgnoreCase(firstNameObject2);
            }
            else{
                if(firstNameObject1.equals("")){
                    return 1;
                }
                return firstNameObject2.compareToIgnoreCase(firstNameObject1);
            }

        }
    }

    /*****************************************************************************************************
     This is a class that is responsible for sorting an arraylist of contacts by their zipcode.
     *****************************************************************************************************/
    private class SortByZipcode implements Comparator<Object[]> {

        @Override
        public int compare(Object[] object1, Object[] object2) {

            String zipcode1 = (String) object1[ContactAttributeIndexer.ZIPCODE.getIndex()];
            String zipcode2 = (String) object2[ContactAttributeIndexer.ZIPCODE.getIndex()];
            String lastNameObject1 = (String) object1[ContactAttributeIndexer.LASTNAME.getIndex()];
            String lastNameObject2 = (String) object2[ContactAttributeIndexer.LASTNAME.getIndex()];
            String firstNameObject1 = (String) object1[ContactAttributeIndexer.FIRSTNAME.getIndex()];
            String firstNameObject2 = (String) object2[ContactAttributeIndexer.FIRSTNAME.getIndex()];

            if(zipcode1.equals("") && zipcode2.equals("")){
                return 0;
            }
            if(zipcode1.equals("")){
                return 1;
            }
            if(zipcode2.equals("")){
                return -1;
            }
            if(!_sortUp){
                zipcode1 = (String) object2[ContactAttributeIndexer.ZIPCODE.getIndex()];
                zipcode2 = (String) object1[ContactAttributeIndexer.ZIPCODE.getIndex()];
                lastNameObject1 = (String) object2[ContactAttributeIndexer.LASTNAME.getIndex()];
                lastNameObject2 = (String) object1[ContactAttributeIndexer.LASTNAME.getIndex()];
                firstNameObject1 = (String) object2[ContactAttributeIndexer.FIRSTNAME.getIndex()];
                firstNameObject2 = (String) object1[ContactAttributeIndexer.FIRSTNAME.getIndex()];
            }
            if(zipcode1.equals(zipcode2)){

                if(lastNameObject1.equals("") && lastNameObject2.equals("")){
                    return 0;
                }
                if(lastNameObject1.equals("")){
                    return 1;
                }
                if(lastNameObject2.equals("")){
                    return -1;
                }
                if(lastNameObject1.equals(lastNameObject2)){
                    if(firstNameObject1.equals("") && firstNameObject2.equals("")){
                        return 0;
                    }
                    if(firstNameObject1.equals("")){
                        return 1;
                    }
                    if(firstNameObject2.equals("")){
                        return -1;
                    }
                    //then we will sort by the first name
                    return firstNameObject1.compareToIgnoreCase(firstNameObject2);

                }
                else{
                    if(lastNameObject1.equals("")){
                        return 1;
                    }
                    //then we will sort by the last name
                    return lastNameObject1.compareToIgnoreCase(lastNameObject2);
                }
            }
            else{
                //then we will sort by zipcode
                return zipcode1.compareToIgnoreCase(zipcode2);
            }
        }

    }

    /*****************************************************************************************************
     This is a class that is responsible for sorting an arraylist of contacts by their primary address.
     *****************************************************************************************************/
    private class SortByAddressLine1 implements Comparator<Object[]> {
        @Override
        public int compare(Object[] object1, Object[] object2) {
            String addressLine1a = (String) object1[ContactAttributeIndexer.STREETADDRESS1.getIndex()];
            String addressLine1b = (String) object2[ContactAttributeIndexer.STREETADDRESS2.getIndex()];
            if(addressLine1a.equals("") && addressLine1b.equals("")){
                return 0;
            }
            if(addressLine1a.equals("")){
                return 1;
            }
            if(addressLine1b.equals("")){
                return -1;
            }
            if(_sortUp){
                if(addressLine1a.equals("")){
                    return 1;
                }
                return addressLine1a.compareToIgnoreCase(addressLine1b);
            }
            else{
                if(addressLine1b.equals("")){
                    return 1;
                }
                return addressLine1b.compareToIgnoreCase(addressLine1a);
            }
        }
    }

    /*****************************************************************************************************
     This is a class that is responsible for sorting an arraylist of contacts by their secondary address.
     *****************************************************************************************************/
    private class SortByAddressLine2 implements Comparator<Object[]> {
        @Override
        public int compare(Object[] object1, Object[] object2) {
            String addressLine1a = (String) object1[ContactAttributeIndexer.STREETADDRESS2.getIndex()];
            String addressLine1b = (String) object2[ContactAttributeIndexer.STREETADDRESS2.getIndex()];
            if(addressLine1a.equals("") && addressLine1b.equals("")){
                return 0;
            }
            if(addressLine1a.equals("")){
                return 1;
            }
            if(addressLine1b.equals("")){
                return -1;
            }
            if(_sortUp){
                if(addressLine1a.equals("")){
                    return 1;
                }
                return addressLine1a.compareToIgnoreCase(addressLine1b);
            }
            else{
                if(addressLine1b.equals("")){
                    return 1;
                }
                return addressLine1b.compareToIgnoreCase(addressLine1a);
            }
        }
    }

    /*****************************************************************************************************
     This is a class that is responsible for sorting an arraylist of contacts by their city.
     *****************************************************************************************************/
    private class SortByCity implements Comparator<Object[]> {
        @Override
        public int compare(Object[] object1, Object[] object2) {
            String city1 = (String) object1[ContactAttributeIndexer.CITY.getIndex()];
            String city2 = (String) object2[ContactAttributeIndexer.CITY.getIndex()];
            if(city1.equals("") && city2.equals("")){
                return 0;
            }
            if(city1.equals("")){
                return 1;
            }
            if(city2.equals("")){
                return -1;
            }
            if(_sortUp){

                return city1.compareToIgnoreCase(city2);
            }
            else{

                return city2.compareToIgnoreCase(city1);
            }



        }
    }

    /*****************************************************************************************************
     This is a class that is responsible for sorting an arraylist of contacts by their state.
     *****************************************************************************************************/
    private class SortByState implements Comparator<Object[]> {
        @Override
        public int compare(Object[] object1, Object[] object2) {
            String state1 = (String) object1[5];
            String state2 = (String) object2[5];
            if(state1.equals("") && state2.equals("")){
                return 0;
            }
            if(state1.equals("")){
                return 1;
            }
            if(state2.equals("")){
                return -1;
            }
            if(_sortUp){
                return state1.compareToIgnoreCase(state2);
            }
            else{
                return state2.compareToIgnoreCase(state1);
            }
        }
    }

    /*****************************************************************************************************
     This is a class that is responsible for sorting an arraylist of contacts by their phone number.
     *****************************************************************************************************/
    private class SortByPhone implements Comparator<Object[]> {
        @Override
        public int compare(Object[] object1, Object[] object2) {
            String phone1 = (String) object1[7];
            String phone2 = (String) object2[7];
            if(phone1.equals("") && phone2.equals("")){
                return 0;
            }
            if(phone1.equals("")){
                return 1;
            }
            if(phone2.equals("")){
                return -1;
            }
            if(_sortUp){
                return phone1.compareToIgnoreCase(phone2);
            }
            else{
                return phone2.compareToIgnoreCase(phone1);
            }
        }
    }

    //todo when reading input from user filter for white space

    /*****************************************************************************************************
     This is a class that is responsible for sorting an arraylist of contacts by their email address.
     *****************************************************************************************************/
    private class SortByEmail implements Comparator<Object[]> {
        @Override
        public int compare(Object[] object1, Object[] object2) {
            String email1 = (String) object1[8];
            String email2 = (String) object2[8];
            if(email1.equals("") && email2.equals("")){
                return 0;
            }
            if(email1.equals("")){
                return 1;
            }
            if(email2.equals("")){
                return -1;
            }
            if(_sortUp){
                return email1.compareToIgnoreCase(email2);
            }
            else{
                return email2.compareToIgnoreCase(email1);
            }
        }
    }

    /*****************************************************************************************************
     This is the method that is responsible for performing the actual sorting based on the Collection class
     and the sorting classes defined above.
     *****************************************************************************************************/
    void sortContacts(ArrayList<Object[]> contactsBook, SortingType sortType, Boolean sortUp){
        _sortUp = sortUp;
        switch (sortType) {
            case FIRSTNAME:
                //then we are going to sort by first name
                Collections.sort(contactsBook, new SortByFirstName());
                break;
            case LASTNAME:
                //then we are going to sort by last name
                Collections.sort(contactsBook, new SortByLastName());
                break;
            case ADDRESSLINE1:
                //then we are going to sort by AddressLine1
                Collections.sort(contactsBook, new SortByAddressLine1());
                break;
            case ADDRESSLINE2:
                //then we are going to sort by AddressLine2
                Collections.sort(contactsBook, new SortByAddressLine2());
                break;
            case CITY:
                //then we are going to sort by city
                Collections.sort(contactsBook, new SortByCity());
                break;
            case STATE:
                //then we are going to sort by state
                Collections.sort(contactsBook, new SortByState());
                break;
            case ZIPCODE:
                //else we are going to sort by zipcode
                Collections.sort(contactsBook, new SortByZipcode());
                break;
            case PHONE:
                //then we are going to sort by phone
                Collections.sort(contactsBook, new SortByPhone());
                break;
            case EMAIL:
                //then we are going to sort by email
                Collections.sort(contactsBook, new SortByEmail());
                break;
            default:
                //then we are going to sort by last name
                Collections.sort(contactsBook, new SortByLastName());
                break;
        }
    }
}
