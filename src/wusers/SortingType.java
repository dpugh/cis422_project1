package wusers;

/*****************************************************************************************************
 This is an enum type that is used to allow the sorting algorithm to know how you would like the
 contactsBook to be sorted.  Using this instead of strings so the user can't enter incorrect sorting
 types
 *****************************************************************************************************/
public enum SortingType {
    FIRSTNAME, LASTNAME, ADDRESSLINE1, ADDRESSLINE2, CITY, STATE, ZIPCODE, PHONE, EMAIL
}
