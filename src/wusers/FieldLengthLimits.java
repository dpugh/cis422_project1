package wusers;

/*
This is an enum that gives the field length limits for contact attributes
 */
public enum FieldLengthLimits {

    FIRSTNAME (25),
    LASTNAME(25),
    STREETADDRESS1(50),
    STREETADDRESS2(25),
    CITY(20),
    STATE(15),
    ZIPCODE(10),
    PHONENUMBER(15),
    EMAIL(20);

    private Integer _index = 0;

    FieldLengthLimits(Integer index){
        _index = index;
    }
    Integer getIndex(){
        return _index;
    }

}
