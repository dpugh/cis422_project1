package wusers;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;


public class ContactsBooksManager {

    //Render the main window and JTable tabs
    private JTabbedPane tabbedPane;
    private JFrame frame;

    //Searchable text fields
    private JTextPane book_title_field;
    private JTextField search_term_field;

    //Store open contactsBooks and search strings
    private java.util.List<ContactsBook> contactsBooks = new ArrayList<>();
    private java.util.List<String> searchStrings = new ArrayList<>();

    //Keeps track of which tab is currently open
    private Integer previousTabIndex;

    private ContactsBooksManager(){
        initialize();
    }

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    ContactsBooksManager window = new ContactsBooksManager();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /*
    Container method for initializing various GUI components and creating initial contact book.
        JFrame, JLabels/JTextFields, JMenu
     */
    private void initialize() {
        initializeFrame();
        initializeUIComponents();
        initializeMenuBar();
        initializeNewContactsBook();
    }

    /*
    Container method for initializing GUIs main window.
     */
    private void initializeFrame() {
        frame = new JFrame();
        frame.getContentPane().setBackground(Color.PINK);
        frame.setBackground(Color.PINK);
        frame.setBounds(100, 100, 1056, 650);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
    }

    /*
    Initializes each menu bar option and registers event handlers.
     */
    private void initializeMenuBar() {
        //Create File menu tab with menu items to be added.
        JMenuBar menuBar = new JMenuBar();
        JMenu file_tab = new JMenu("File");
        JMenuItem newMenuItem = new JMenuItem("New");
        JMenuItem open = new JMenuItem("Open");
        JMenuItem close = new JMenuItem("Close Book");
        JMenuItem save = new JMenuItem("Save");
        JMenuItem saveAs = new JMenuItem("Save As");
        JMenuItem exit = new JMenuItem("Exit");

        //Add event handlers for each File menu option.
        newMenuItem.addActionListener(new ContactsBooksManager.NewContactsBookEventHandler());
        open.addActionListener(new ContactsBooksManager.OpenContactsBookEventHandler());
        close.addActionListener(new ContactsBooksManager.CloseContactsBookTab());
        save.addActionListener(new ContactsBooksManager.SaveContactsBookEventHandler());
        saveAs.addActionListener(new ContactsBooksManager.SaveAsContactsBookEventHandler());
        exit.addActionListener(new ContactsBooksManager.ExitContactsBookEventHandler());

        //Add each menu item to the parent tab.
        file_tab.add(newMenuItem);
        file_tab.add(open);
        file_tab.add(save);
        file_tab.add(saveAs);
        file_tab.add(close);
        file_tab.add(exit);

        //Create Tools menu tab with menu items to be added.
        JMenu tools = new JMenu("Tools");
        JMenuItem exportButton = new JMenuItem("Export");
        JMenuItem importButton = new JMenuItem("Import");

        //Add event handlers for each Tools menu option.
        exportButton.addActionListener(new ContactsBooksManager.ExportContactsBookEventHandler());
        importButton.addActionListener(new ContactsBooksManager.ImportContactsBookEventHandler());
        tools.add(importButton);
        tools.add(exportButton);

        //Finalize menu bar with tabs
        menuBar.add(file_tab);
        menuBar.add(tools);
        frame.getContentPane().setLayout(null);
        frame.setJMenuBar(menuBar);
    }

    /*
    Initializes various labels and textfields for the GUI.
     */
    private void initializeUIComponents(){
        //Initialize the field for the contact book title.
        book_title_field = new JTextPane();
        book_title_field.setBounds(10, 10, 400, 25);

        //Initialize the tab for the initial contact book table.
        tabbedPane = new JTabbedPane();
        tabbedPane.setBounds(10, 50,1036, 493+50);
        tabbedPane.addMouseListener(new TabbedPaneEventHandler());
        frame.add(tabbedPane);

        //Initialize search label and editable search field
        JLabel search_label;
        search_label = new JLabel("Search:");
        search_label.setBounds(675, 10, 60, 25);
        search_term_field = new JTextField();
        search_term_field.setBounds(675+52, 10, 150, 25);
        search_term_field.getDocument().addDocumentListener(new SearchTermChangedEventHandler());

        //Initialize add contact button and add event handler.
        JButton add_contact_button;
        add_contact_button = new JButton("Add Contact");
        add_contact_button.setBounds(890, 10, 150, 25);
        add_contact_button.addActionListener(new AddContactEventHandler());

        //Add components to the frame of the window
        frame.getContentPane().add(book_title_field);
        frame.getContentPane().add(search_label);
        frame.getContentPane().add(search_term_field);
        frame.getContentPane().add(add_contact_button);
    }

    /*
    Create a new ContactsBook for the GUI to display in a tab.
     */
    private void initializeNewContactsBook(){
        //Instantiate new ContactsBook, add to program execution
        ContactsBook contactsBook = new ContactsBook();
        initializeContactBook(contactsBook);
    }

    /*
    Get search string from search_field from current open contactsBook tab.
     */
    private String getSearchString(){
       try{
           return searchStrings.get(contactsBooks.size() - 1);
       }
       catch(Exception e){
           return "";
       }
    }

    /*
    Fills JTable with contact book data, and then create a new tab to display the data.
     */
    private void initializeContactBook(ContactsBook contactsBook){

        //Load contacts book to be displayed in JTable
        contactsBook.loadJTableWithData(contactsBook.book.getContactsBook());

        //Add the contacts book to the contactsBooks array list for the GUI
        contactsBooks.add(contactsBook);

        //Add contacts book to new tabbed panel.
        JPanel jpanel = new JPanel();
        jpanel.setBounds(100, 100, 1156, 750);
        jpanel.add(contactsBook.tableScrollPanel);
        String name = contactsBook.book.getBookName();
        tabbedPane.add(name, contactsBook.tableScrollPanel);
        searchStrings.add("");
        book_title_field.setText(contactsBook.book.getBookName());
        tabbedPane.setSelectedIndex(tabbedPane.getTabCount() - 1);
    }

    /*
    Used for keeping track of which contactsBook tab is currently open.
     */
    private void removePreviousTabIndexIfPreviousTabIsClosed(Integer index){
        if(previousTabIndex == null){
            return;
        }
        if(index.equals(previousTabIndex)){
            previousTabIndex = null;
        }
    }

    /*
    Gets the index of the currently selected JTable tab.
     */
    private ContactsBook getContactsBookForCurrentTab() throws ArrayIndexOutOfBoundsException{
        int currentIndex = tabbedPane.getSelectedIndex();
        return contactsBooks.get(currentIndex);

    }

    /*
    Changes tab name at a given index.
     */
    private void changeTabName(Integer index, String name){
        tabbedPane.setTitleAt(index, name);
    }

    /*
    Save current contactsBook to a file.
     */
    private void saveContactsBookAs(){
        //Retrieve contacts book for current tab.
        ContactsBook contactsBook = getContactsBookForCurrentTab();
        contactsBook.setContactsBookFromJTableModel();

        //Check if save path and file name have already been set
        String pathAndFileName = contactsBook.chooseFilePathAndNameForSave("", false);
        if (pathAndFileName == null) {
            //throw new IOException();
            return;
        }

        //Check to see if a file already exists
        File f = new File(pathAndFileName);
        if (f.exists() && !f.isDirectory()) {

            //If file already exists, ask them if they want to save over it
            Boolean confirmResult = contactsBook.renderConfirmationModal("Confirm", "A file with that name already exists.  Overwrite?");
            if (!confirmResult) {
                ContactsBooksManager.SaveAsContactsBookEventHandler saveAsContactsBookEventHandler = new ContactsBooksManager.SaveAsContactsBookEventHandler();
                saveAsContactsBookEventHandler.actionPerformed(null);
                return;
            }
        }

        //Set book name, keep track of file path, and save
        contactsBook.book.setBookName(book_title_field.getText());
        contactsBook.book.setBookFileNameAndPath(pathAndFileName);
        try {
            contactsBook.book.saveBook();
            contactsBook.altered = false;
        } catch (IOException e) {
            contactsBook.renderErrorMessageModal("There was a problem saving your file.  This is most likely due to permissions issues.");
        }
    }

    /*
    Save current contact book, throw exception if unable.
     */
    private void saveContactsBook(){
        //Retrieve contacts book for current tab.
        ContactsBook contactsBook = getContactsBookForCurrentTab();
        contactsBook.setContactsBookFromJTableModel();
        try {
            //Save the file from where the file was opened or if this is a new file then the user will get a UI which has them choose where to save the file
            contactsBook.book.setBookName(book_title_field.getText());
            contactsBook.setBookNamePathAndSave();
            contactsBook.altered = false;
        } catch (IOException e) {
            //Call error message modal to let them know the file could not be saved here most likely because of file permission issues.
            contactsBook.renderErrorMessageModal("There was a problem saving your file.  This is most likely due to file permission issues.  Check to make sure you have permission to save the file at this location");
        }
    }

    /*
    Action listener for adding contacts via add contact modal.
     */
    private class AddContactEventHandler implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            //Get current tab of JTable and attempt to add a contact.
            int index = tabbedPane.getSelectedIndex();
            ContactsBook contactsBook = contactsBooks.get(index);
            contactsBook.addContactTriggerModal();
        }
    }

    /*
    Event handler called when closing a contactsBook. Attempts to save
    the contactsBook before closing. Updates the tab count upon finishing.
     */
    private class CloseContactsBookTab implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            //Attempt to save current contactsBook, or report failure to user.

            ContactsBook contactsBook = contactsBooks.get(tabbedPane.getSelectedIndex());
            if(contactsBook.altered){
                saveContactsBookAs();
            }

            //Update number of tabs currently open, and remove current tab.
            int index = tabbedPane.getSelectedIndex();
            removePreviousTabIndexIfPreviousTabIsClosed(index);
            tabbedPane.remove(index);
            contactsBooks.remove(index);
            searchStrings.remove(index);
        }
    }

    /*
    Event handler that allows user to search contact book.
    Each method gets text from search field, and searches for matching text
    within the current contact book.
     */
    private class SearchTermChangedEventHandler implements DocumentListener{
        /*
        Each method gets text from search field, and searches for matching text
        within the current contact book.
         */
        @Override
        public void insertUpdate(DocumentEvent documentEvent) {
            String searchTerm = search_term_field.getText();
            Integer index = tabbedPane.getSelectedIndex();
            searchStrings.set(index, searchTerm);
            ContactsBook contactsBook = contactsBooks.get(index);
            contactsBook.search(searchTerm);
        }
        @Override
        public void removeUpdate(DocumentEvent documentEvent) {
            String searchTerm = search_term_field.getText();
            Integer index = tabbedPane.getSelectedIndex();
            ContactsBook contactsBook = contactsBooks.get(index);
            contactsBook.search(searchTerm);
        }
        @Override
        public void changedUpdate(DocumentEvent documentEvent) {
            String searchTerm = search_term_field.getText();
            Integer index = tabbedPane.getSelectedIndex();
            ContactsBook contactsBook = contactsBooks.get(index);
            contactsBook.search(searchTerm);
        }
    }

    /*
    This event handler is used when a user clicks on a tab to load the contactsbook for that tab.
    The search data term and data are loaded if applicable and the table is loaded
     */
    private class TabbedPaneEventHandler implements MouseListener{

        @Override
        public void mouseClicked(MouseEvent mouseEvent) {
            //we want to set the tab we just left with the contacts book text panel that they might have changed
            if(previousTabIndex != null)
            {
                String searchTermFieldString = search_term_field.getText();
                searchStrings.set(previousTabIndex, searchTermFieldString);
                //also set the book's name to be the name of the book_title_field
                ContactsBook contactsBook = contactsBooks.get(previousTabIndex);
                String shortenedName = getContactBookNameForCurrentTab();
                contactsBook.book.setBookName(book_title_field.getText());
                //also set the tab
                changeTabName(previousTabIndex, shortenedName);
            }

            Integer index = tabbedPane.getSelectedIndex();
            System.out.println(index);

            //need to reload the add_contact_field and the add_contact_button
            ContactsBook contactsBook = contactsBooks.get(index);
            String bookName = contactsBook.book.getBookName();
            book_title_field.setText(bookName);
            search_term_field.setText(searchStrings.get(index));

            previousTabIndex = index;

        }

        @Override
        public void mousePressed(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseReleased(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseEntered(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseExited(MouseEvent mouseEvent) {

        }
    }

    /*
    Returns contact book name from the contact book on the current tab.
     */
    private String getContactBookNameForCurrentTab() {
        String contactBookName = book_title_field.getText();
        contactBookName = contactBookName.substring(0, Math.min(contactBookName.length(), 20)) + "...";
        return contactBookName;
    }

    /*
    Event handler triggered when new contactsBook is created.
     */
    private class NewContactsBookEventHandler implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            ContactsBook contactsBook = new ContactsBook();
            initializeContactBook(contactsBook);
        }
    }

    /*
     Event handler for File>Open menu item. Attempts to open user-specified file
     and load its contents into a JTable displayed to the user.
     */
    private class OpenContactsBookEventHandler implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            //Create new contact book
            ContactsBook contactsBook = new ContactsBook();
            try {
                //Attempt to open contactsBook specified by user.
                JFileChooser fileChooser = new JFileChooser();
                Integer result = fileChooser.showOpenDialog(ContactsBooksManager.this.frame);
                if(result == JFileChooser.CANCEL_OPTION) {
                    return;
                } else if(result == JFileChooser.ERROR_OPTION){
                    throw new Exception();
                }

                //If successful, open the file and set the file path.
                File file = fileChooser.getSelectedFile();
                contactsBook.book.setBookFileNameAndPath(file.toString());
                searchStrings.add("");

                //Set the title from the contactsBook and load JTable with the data from specified contactsBook file
                contactsBook.book.getBookFromFile();
                String contactBookName = contactsBook.book.getBookName();
                contactsBook.bookTitleField.setText(contactBookName);
                java.util.ArrayList<Object[]> contactsBookList = contactsBook.book.getContactsBook();
                contactsBook.loadJTableWithData(contactsBookList);
                book_title_field.setText(contactBookName);

                //Create a new tab for the JTable to hold our new contactsBook
                contactBookName = contactBookName.substring(0, Math.min(contactBookName.length(), 20)) + "...";
                tabbedPane.add(contactBookName, contactsBook.tableScrollPanel);

                //Add newly opened contactsBook to the data structure which holds all of the contactBooks
                contactsBooks.add(contactsBook);
                tabbedPane.setSelectedIndex(tabbedPane.getTabCount() - 1);
                String searchTerm = getSearchString();
                search_term_field.setText(searchTerm);
            } catch (Exception e) {
                contactsBook.renderErrorMessageModal("There was a problem opening your file.  Either the file does not exist, is corrupted or is not a Contacts Book file format.");
            }
        }
    }

    /*
    Event handler for Tools>Export menu item. Attempts to open user-specified file
    location and write the current contactsBook as a .tsv file.
     */
    private class ExportContactsBookEventHandler implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            try{
                //Determine which contactsBook is open
                int currentBookIndex = tabbedPane.getSelectedIndex();
                ContactsBook contactsBook = contactsBooks.get(currentBookIndex);

                //Get JTable data, and prepare to export with pseudo-random file name
                contactsBook.setContactsBookFromJTableModel();
                String contactsBookName = contactsBook.book.getBookName();
                contactsBookName = contactsBookName.replaceAll("\\s+","");
                Random r = new Random();
                int randomInteger = r.nextInt(1000000);
                contactsBookName += "_export_" + randomInteger + ".tsv";

                //Create a new file using contactsBook, and attempt to save a as export file format
                File file = new File(contactsBookName);
                JFileChooser fc = new JFileChooser();
                fc.setSelectedFile(file);
                int returnValue = fc.showSaveDialog(ContactsBooksManager.this.frame);
                if(returnValue == JFileChooser.CANCEL_OPTION) {
                    return;
                } else if(returnValue == JFileChooser.ERROR_OPTION){
                    throw new IOException();
                }

                //Get a handle on specified file
                File selectedFile = fc.getSelectedFile();

                //Check to see if a file already exists
                File f = new File(selectedFile.toString());
                if(f.exists() && !f.isDirectory()) {
                    //If so, ask user if they want to overwrite it
                    Boolean confirmResult = contactsBook.renderConfirmationModal("Confirm", "A file with that name already exists.  Overwrite?");
                    if(!confirmResult){
                        ContactsBooksManager.ExportContactsBookEventHandler exportContactsBookEventHandler = new ContactsBooksManager.ExportContactsBookEventHandler();
                        exportContactsBookEventHandler.actionPerformed(null);
                        return;
                    }
                }

                //If all is good, export the contactsBook
                contactsBook.book.setBookExportNameAndPath(selectedFile.toString());
                contactsBook.book.exportContactsBook();
            } catch (Exception e){
                ContactsBook contactsBook = new ContactsBook();
                contactsBook.renderErrorMessageModal("There was a problem opening your file.  Either the file does not exist, is corrupted or is not a Contacts Book file format.");
            }
        }
    }

    /*
    Event handler for Tools>Import menu item. Attempts to open user-specified file
    location and read a .tsv file into a new contactsBook.
     */
    private class ImportContactsBookEventHandler implements  ActionListener{
        @Override
        public void actionPerformed(ActionEvent actionEvent) {

            //Instantiate new contactsBook to hold imported data
            ContactsBook contactsBook = new ContactsBook();

            try{
                //Declare File object, get export name and path.
                File file;
                String contactsBookExportNameAndPath = contactsBook.book.getBookExportNameAndPath();

                //If there is an export path, open file at that location. Else, open predefined filename.
                if(contactsBookExportNameAndPath != null && !contactsBookExportNameAndPath.equals("")){
                    file = new File(contactsBookExportNameAndPath);
                } else{
                    file = new File("mycontactbook_export.tsv");
                }

                //Ask user to select a file to import.
                JFileChooser fc = new JFileChooser();
                fc.setSelectedFile(file);
                int returnValue = fc.showOpenDialog(ContactsBooksManager.this.frame);
                if(returnValue == JFileChooser.CANCEL_OPTION){
                    return;
                } else if(returnValue == JFileChooser.ERROR_OPTION){
                    throw new Exception();
                }

                //Get the selected file, and load the data into the new contactsBook
                File selectedFile = fc.getSelectedFile();
                contactsBook.book.setBookExportNameAndPath(selectedFile.toString());
                contactsBook.book.importContactsBook();
                contactsBook.loadJTableWithData(contactsBook.book.getContactsBook());
                contactsBook.bookTitleField.setText(contactsBook.book.getBookName());

                //Add the new contactsBook to the arraylist contactsBooks, then add to JTable tab.
                contactsBooks.add(contactsBook);
                tabbedPane.add(contactsBook.book.getBookName(), contactsBook.tableScrollPanel);

                //Update tab count and selected tab
                tabbedPane.setSelectedIndex(tabbedPane.getTabCount() - 1);
                searchStrings.add("");
            } catch (Exception e){
                contactsBook.renderErrorMessageModal("There was a problem importing your file.  Either the file does not exist, is corrupted or is not a Contacts Book import file 'tsv' format.");
            }
        }
    }

    /*
    Event handler for File>Save menu item. Calls contactsBook save method.
     */
    private class SaveContactsBookEventHandler implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent actionEvent)
        {
            saveContactsBook();
        }
    }
    /*
    Event handler for File>Save As menu item. Calls contactsBook save method.
     */
    private class SaveAsContactsBookEventHandler implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
                saveContactsBookAs();
        }
    }

    /*
    Event handler for File>Exit menu item. Attempts to save and close each open
    contactsBook before exiting the program.
     */
    private class ExitContactsBookEventHandler implements  ActionListener{
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            //Iterate over each tab and perform a save as operation on each.
           for(int i = tabbedPane.getTabCount() - 1; i > -1; i--){
                tabbedPane.setSelectedIndex(i);
               ContactsBook contactsBook = contactsBooks.get(i);
               if(contactsBook.altered){
                   saveContactsBookAs();
               }
               //Close tab after saving
               tabbedPane.remove(i);
           }
           //Exit after all tabs have been closed
            System.exit(0);
        }
    }
}
