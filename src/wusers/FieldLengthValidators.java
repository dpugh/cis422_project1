package wusers;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;

/*this is a container class that contains a set of inner classes that manager the validation
on the various textPanels used in the project
*/
class FieldLengthValidators {

    static class FirstNameLengthValidator extends DefaultStyledDocument {
        @Override
        public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
            if ((getLength() + str.length()) <= FieldLengthLimits.FIRSTNAME.getIndex()) {
                super.insertString(offs, str, a);
            }
        }
    }
    static class LastNameLengthValidator extends DefaultStyledDocument {
        @Override
        public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
            if ((getLength() + str.length()) <= FieldLengthLimits.LASTNAME.getIndex()) {
                super.insertString(offs, str, a);
            }
        }
    }
    static class AddressLine1LengthValidator extends DefaultStyledDocument {
        @Override
        public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
            if ((getLength() + str.length()) <= FieldLengthLimits.STREETADDRESS1.getIndex()) {
                super.insertString(offs, str, a);
            }
        }
    }
    static class AddressLine2LengthValidator extends DefaultStyledDocument {
        @Override
        public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
            if ((getLength() + str.length()) <= FieldLengthLimits.STREETADDRESS2.getIndex()) {
                super.insertString(offs, str, a);
            }
        }
    }
    static class CityNameLengthValidator extends DefaultStyledDocument {
        @Override
        public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
            if ((getLength() + str.length()) <= FieldLengthLimits.CITY.getIndex()) {
                super.insertString(offs, str, a);
            }
        }
    }
    static class StateLengthValidator extends DefaultStyledDocument {
        @Override
        public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
            if ((getLength() + str.length()) <= FieldLengthLimits.STATE.getIndex()) {
                super.insertString(offs, str, a);
            }
        }
    }
    static class ZipcodeLengthValidator extends DefaultStyledDocument {
        @Override
        public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
            if ((getLength() + str.length()) <= FieldLengthLimits.ZIPCODE.getIndex()) {
                super.insertString(offs, str, a);
            }
        }
    }
    static class PhoneNumberLengthValidator extends DefaultStyledDocument {
        @Override
        public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
            if ((getLength() + str.length()) <= FieldLengthLimits.PHONENUMBER.getIndex()) {
                super.insertString(offs, str, a);
            }
        }
    }
    static class EmailValidator extends DefaultStyledDocument {
        @Override
        public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
            if ((getLength() + str.length()) <= FieldLengthLimits.EMAIL.getIndex()) {
                super.insertString(offs, str, a);
            }
        }
    }
}
