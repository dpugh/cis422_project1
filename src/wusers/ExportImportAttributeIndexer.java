package wusers;

/*
This is an enum that takes a particular contact attribute and gives you the corresponding index for the contacts
attribute array for importing or exporting
 */
public enum ExportImportAttributeIndexer {
    CITY (0),
    STATE(1),
    ZIPCODE(2),
    STREETADDRESS1(3),
    STREEADDRESS2(4),
    LASTNAME(5),
    FIRSTNAME(6),
    PHONENUMBER(7);

    private Integer _index = 0;

    ExportImportAttributeIndexer(Integer index){
        _index = index;
    }
    Integer getIndex(){
        return _index;
    }
}
