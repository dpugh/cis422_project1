package wusers;

import java.io.*;
import java.util.ArrayList;

/*
Book is a class that holds all of the contact data as well as performs file read write
operations.
 */
public class Book {

    private String _bookName = "My Contact Book";
    private String _bookFileNameAndPath;
    private String _bookExportNameAndPath;
    private ArrayList<Object[]> _contactsBook = new ArrayList<>();

    private ArrayList<Object[]> _searchedContactsBook = new ArrayList<>();

    /*****************************************************************************************************
     This is a constructor that is used for contact books that already exist within the file system.
     Pass in the file's path/name and the constructor sets the filename/path, then extracts the data from
     the file.
     *****************************************************************************************************/
    public Book(String bookFileNameAndPath) throws Exception {
        setBookFileNameAndPath(bookFileNameAndPath);
        getBookFromFile();
    }
    public Book(){}

    /*****************************************************************************************************
     This method is responsible for fetching contacts from a contactsBook file.  The method reads the data
     from the file, and puts that data into an arraylist of object arrays.
     *****************************************************************************************************/
    void getBookFromFile() throws Exception {

        FileReader fileReader = new FileReader(getBookFileNameAndPath());
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String bookName = bufferedReader.readLine();
        setBookName(bookName);
        String currentLine;
        _contactsBook = new ArrayList<>();
        while((currentLine = bufferedReader.readLine()) != null){

            int success = processLineFromContactBook(currentLine);
            if(success == 0){
                throw new Exception("Error processing Contacts Book file.");
            }
        }
    }

    /*****************************************************************************************************
     This is the method that is responsible for saving the address book from the data member contactsBook
     back to the contacts book file to the location specified by the member variable _bookFileNameAndPath
     *****************************************************************************************************/
    void saveBook() throws IOException {

        FileWriter fileWriter = new FileWriter(getBookFileNameAndPath());

        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        String bookName = getBookName();
        bufferedWriter.write(bookName);
        bufferedWriter.newLine();

        for(int i = 0; i < getContactsBook().size(); i++){

            Object[] currentAddress = getContactsBook().get(i);
            String currentAddressString = getAddressStringFromAddress(currentAddress);
            bufferedWriter.write(currentAddressString);
            bufferedWriter.newLine();
        }
        bufferedWriter.close();

        //todo flush and close the filewriter

    }
    private String getContactAttributeByIndex(ContactAttributeIndexer contactAttributeIndexer, Object[] contact){

        String attribute = (String) contact[contactAttributeIndexer.getIndex()];
        if(attribute == null){
            return "";
        }
        return attribute;
    }
    /*****************************************************************************************************
     This method is responsible for getting a contact string from a contact object array.  This string is
     used to write a line to the contacts file.
     *****************************************************************************************************/
    private String getAddressStringFromAddress(Object[] currentAddress) {



        return  getContactAttributeByIndex(ContactAttributeIndexer.FIRSTNAME, currentAddress) + "," +
                getContactAttributeByIndex(ContactAttributeIndexer.LASTNAME, currentAddress) + "," +
                getContactAttributeByIndex(ContactAttributeIndexer.STREETADDRESS1, currentAddress)  + "," +
                getContactAttributeByIndex(ContactAttributeIndexer.STREETADDRESS2, currentAddress)  + "," +
                getContactAttributeByIndex(ContactAttributeIndexer.CITY, currentAddress)  + "," +
                getContactAttributeByIndex(ContactAttributeIndexer.STATE, currentAddress)  + "," +
                getContactAttributeByIndex(ContactAttributeIndexer.ZIPCODE, currentAddress) + "," +
                getContactAttributeByIndex(ContactAttributeIndexer.PHONENUMBER, currentAddress) + "," +
                getContactAttributeByIndex(ContactAttributeIndexer.EMAIL, currentAddress)  + "," +
                getContactAttributeByIndex(ContactAttributeIndexer.FACEBOOK, currentAddress)  + "," +
                getContactAttributeByIndex(ContactAttributeIndexer.TWITTER, currentAddress)  + "," +
                getContactAttributeByIndex(ContactAttributeIndexer.INSTAGRAM, currentAddress);

    }
    /*****************************************************************************************************
     This method is responsible for getting a contact string from a contact object array.  This string is
     used to write a line to the contacts file.
     *****************************************************************************************************/
    private String getAddressStringFromAddressForExport(Object[] currentAddress) {

        return  currentAddress[ContactAttributeIndexer.CITY.getIndex()] + "\t" +
                currentAddress[ContactAttributeIndexer.STATE.getIndex()] + "\t" +
                currentAddress[ContactAttributeIndexer.ZIPCODE.getIndex()] + "\t" +
                currentAddress[ContactAttributeIndexer.STREETADDRESS1.getIndex()] + "\t" +
                currentAddress[ContactAttributeIndexer.STREETADDRESS2.getIndex()] + "\t" +
                currentAddress[ContactAttributeIndexer.LASTNAME.getIndex()] + "\t" +
                currentAddress[ContactAttributeIndexer.FIRSTNAME.getIndex()] + "\t" +
                currentAddress[ContactAttributeIndexer.PHONENUMBER.getIndex()];
    }
    /*****************************************************************************************************
     This method is responsible for taking a single line from the file being read and processing that line,
     putting it's data into an object array
     *****************-************************************************************************************/
    private int processLineFromContactBook(String currentLine) {

        String[] splitArray = currentLine.split(",", -1);

        if(splitArray.length < 11){  //todo is 8 the correct  #?
            //then the contact book file is corrupted, and we should let them know and quit
            //todo what is best way to let user know their book is corrupted
            return 0;
        }

        String firstName = splitArray[ContactAttributeIndexer.FIRSTNAME.getIndex()];
        String lastName = splitArray[ContactAttributeIndexer.LASTNAME.getIndex()];
        String streetAddress = splitArray[ContactAttributeIndexer.STREETADDRESS1.getIndex()];
        String streetAddress2 = splitArray[ContactAttributeIndexer.STREETADDRESS2.getIndex()];
        String city = splitArray[ContactAttributeIndexer.CITY.getIndex()];
        String state = splitArray[ContactAttributeIndexer.STATE.getIndex()];
        String zipCode = splitArray[ContactAttributeIndexer.ZIPCODE.getIndex()];
        String phoneNumber = splitArray[ContactAttributeIndexer.PHONENUMBER.getIndex()];
        String email = splitArray[ContactAttributeIndexer.EMAIL.getIndex()];
        String facebook = splitArray[ContactAttributeIndexer.FACEBOOK.getIndex()];
        String twitter = splitArray[ContactAttributeIndexer.TWITTER.getIndex()];
        String instragram = splitArray[ContactAttributeIndexer.INSTAGRAM.getIndex()];

        Object[] contact = {firstName, lastName, streetAddress, streetAddress2, city, state, zipCode, phoneNumber, email, facebook, twitter, instragram,};
        getContactsBook().add(contact);
        return 1;
    }

    /*****************************************************************************************************
     This method is responsible for taking a single line from the import file being read and processing that line,
     putting it's data into an object array
     *****************-************************************************************************************/
    private int processLineFromImportFile(String currentLine) {

        String[] splitArray = currentLine.split("\t", -1);

        if(splitArray.length < 7){
            return 0;
        }

        String firstName = splitArray[ExportImportAttributeIndexer.FIRSTNAME.getIndex()];
        String lastName = splitArray[ExportImportAttributeIndexer.LASTNAME.getIndex()];
        String streetAddress = splitArray[ExportImportAttributeIndexer.STREETADDRESS1.getIndex()];
        String streetAddress2 = splitArray[ExportImportAttributeIndexer.STREEADDRESS2.getIndex()];
        String city = splitArray[ExportImportAttributeIndexer.CITY.getIndex()];
        String state = splitArray[ExportImportAttributeIndexer.STATE.getIndex()];
        String zipCode = splitArray[ExportImportAttributeIndexer.ZIPCODE.getIndex()];
        String phoneNumber = splitArray[ExportImportAttributeIndexer.PHONENUMBER.getIndex()];

        Object[] contact = {firstName, lastName, streetAddress, streetAddress2, city, state, zipCode, phoneNumber, "", "", "", ""};
        getContactsBook().add(contact);
        return 1;
    }

    /*****************************************************************************************************
     This is a simple accessor method
     *****************************************************************************************************/
    String getBookName() {
        return _bookName;
    }

    /*****************************************************************************************************
     This is a simple mutator method
     *****************************************************************************************************/
    void setBookName(String bookName) {
        _bookName = bookName;
    }


    /*****************************************************************************************************
     This is a simple accessor method
     *****************************************************************************************************/
    String getBookFileNameAndPath() {
        return _bookFileNameAndPath;
    }

    /*****************************************************************************************************
     This is a simple mutator method
     *****************************************************************************************************/
    void setBookFileNameAndPath(String bookFileNameAndPath) {
        _bookFileNameAndPath = bookFileNameAndPath;
    }

    /*****************************************************************************************************
     This is a simple accessor method
     *****************************************************************************************************/
    ArrayList<Object[]> getContactsBook() {
        return _contactsBook;
    }
    /*****************************************************************************************************
     This is a simple mutator method
     *****************************************************************************************************/
    void setContactsBook(ArrayList<Object[]> contactsBook) {
        _contactsBook = contactsBook;
    }

    /*****************************************************************************************************
     This is a simple accessor method
     *****************************************************************************************************/
    public String getBookExportNameAndPath() {
        return _bookExportNameAndPath;
    }

    /*****************************************************************************************************
     This is a simple mutator method
     *****************************************************************************************************/
    public void setBookExportNameAndPath(String _bookExportNameAndPath) {
        this._bookExportNameAndPath = _bookExportNameAndPath;
    }

    /*****************************************************************************************************
     This method performs the export to file operation for a data export
     *****************************************************************************************************/
    void exportContactsBook() throws IOException{
        FileWriter fileWriter = new FileWriter(getBookExportNameAndPath());
        String headerLine = "CITY\tSTATE\tZIP\tDelivery\tSecond\tLastName\tFirstName\tPhone";
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        //first write the header line which contains the book title
        bufferedWriter.write(headerLine);
        bufferedWriter.newLine();
        //print each contact
        for(int i = 0; i < getContactsBook().size(); i++){

            Object[] currentAddress = getContactsBook().get(i);
            String currentAddressString = getAddressStringFromAddressForExport(currentAddress);
            bufferedWriter.write(currentAddressString);
            bufferedWriter.newLine();
        }
        bufferedWriter.close();

    }
    /*****************************************************************************************************
     This is the method that performs the import on the contacts book
     *****************************************************************************************************/
    void importContactsBook() throws IOException{
        FileReader fileReader = new FileReader(getBookExportNameAndPath());
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        setBookName("My Imported Contact Book");
        bufferedReader.readLine();
        String currentLine;
        _contactsBook = new ArrayList<>();
        while((currentLine = bufferedReader.readLine()) != null){

            int success = processLineFromImportFile(currentLine);
            if(success == 0){
                throw new IOException("Error processing Contacts Book file.");
            }
        }

    }
    /*****************************************************************************************************
     This is the method that will, using a search string, return a subset of those contacts in the model
     that have the search term in one of their fields
     *****************************************************************************************************/
    public void search(String searchTerm) {
        _searchedContactsBook = new ArrayList<>();
        //iterate over _booksList
        for(Object[] currentContact: _contactsBook){
            //str1.toLowerCase().contains(str2.toLowerCase())
            String firstName = (String) currentContact[ContactAttributeIndexer.FIRSTNAME.getIndex()];
            String lastName = (String) currentContact[ContactAttributeIndexer.LASTNAME.getIndex()];
            String addressLine1 = (String) currentContact[ContactAttributeIndexer.STREETADDRESS1.getIndex()];
            String addressLine2 = (String) currentContact[ContactAttributeIndexer.STREETADDRESS2.getIndex()];
            String city = (String) currentContact[ContactAttributeIndexer.CITY.getIndex()];
            String state = (String) currentContact[ContactAttributeIndexer.STATE.getIndex()];
            String zipcode = (String) currentContact[ContactAttributeIndexer.ZIPCODE.getIndex()];
            String phone = (String) currentContact[ContactAttributeIndexer.PHONENUMBER.getIndex()];
            String email = (String) currentContact[ContactAttributeIndexer.EMAIL.getIndex()];

            if(firstName.toLowerCase().contains(searchTerm.toLowerCase()) ||
                    lastName.toLowerCase().contains(searchTerm.toLowerCase()) ||
                    addressLine1.toLowerCase().contains(searchTerm.toLowerCase()) ||
                    addressLine2.toLowerCase().contains(searchTerm.toLowerCase()) ||
                    city.toLowerCase().contains(searchTerm.toLowerCase()) ||
                    state.toLowerCase().contains(searchTerm.toLowerCase()) ||
                    zipcode.toLowerCase().contains(searchTerm.toLowerCase()) ||
                    phone.toLowerCase().contains(searchTerm.toLowerCase()) ||
                    email.toLowerCase().contains(searchTerm.toLowerCase())
            ){
                _searchedContactsBook.add(currentContact);
            }


        }

    }
    /*****************************************************************************************************
     This is a simple accessor method
     *****************************************************************************************************/
    public ArrayList<Object[]> getSearchedContactsBook() {
        return _searchedContactsBook;
    }


}
