package wusers;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.border.EtchedBorder;
import javax.swing.table.JTableHeader;

/*
The purpose of this class is to hold a Book member variable which will hold all of our contact records.
The class also will perform operations on the contacts data in order to view the data fitting the
viewing criteria.  The data set can be sorted, as well as searched.
 */

public class ContactsBook {

	Boolean altered = false;
	JFrame frame; //The main window of the application
	JTable table;
	JScrollPane tableScrollPanel;

	JButton add_contact_button;
	//private JButton edit_contact_button;
	private JButton delete_contact_button;

	private JButton create_button;
	private JButton edit_button;
	private JButton save_button;
	private JButton cancel_button;
	//these are our text panes for our add contact popup modal
	private JTextPane firstname_field;
	private JTextPane lastname_field;
	private JTextPane address_line1_field;
	private JTextPane address_line2_field;
	private JTextPane city_field;
	private JTextPane state_field;
	private JTextPane zipcode_field;
	private JTextPane phone_field;
	private JTextPane email_field;
	private JTextPane facebook_field;
	private JTextPane instagram_field;
	private JTextPane twitter_field;
	//these are our text panes for our contact edit popup modal
	private JTextPane firstname_field_edit;
	private JTextPane lastname_field_edit;
	private JTextPane address_line1_field_edit;
	private JTextPane address_line2_field_edit;
	private JTextPane city_field_edit;
	private JTextPane state_field_edit;
	private JTextPane zipcode_field_edit;
	private JTextPane phone_field_edit;
	private JTextPane email_field_edit;
	private JTextPane facebook_field_edit;
	private JTextPane instagram_field_edit;
	private JTextPane twitter_field_edit;

	//these are our jtextpane's for our contact view popup modal
	private JTextPane firstname_view_pane;
	private JTextPane lastname_view_pane;
	private JTextPane address_line1_view_pane;
	private JTextPane address_line2_view_pane;
	private JTextPane city_view_pane;
	private JTextPane state_view_pane;
	private JTextPane zipcode_view_pane;
	private JTextPane phone_view_pane;
	private JTextPane email_view_pane;
	private JTextPane facebook_view_pane;
	private JTextPane instagram_view_pane;
	private JTextPane twitter_view_pane;

	JScrollPane contactNameAndAddContactScrollPanel;
	JPanel contactsBookFinalLayout;

	JTextPane bookTitleField;

	private JLabel label_error;
	private JLabel label_error_edit;
	private JLabel facebookJLabel;

	private SortingType sortingType = SortingType.LASTNAME;
	private String _searchTerm = "";

	//this is for the sorting, ascending or descending for each of the table headers
	private Boolean _firstNameHeaderSortUp = true;
	private Boolean _lastNameHeaderSortUp = true;
	private Boolean _addressLine1HeaderSortUp = true;
	private Boolean _addressLine2HeaderSortUp = true;
	private Boolean _cityHeaderSortUp = true;
	private Boolean _stateHeaderSortUp = true;
	private Boolean _zipcodeHeaderSortUp = true;
	private Boolean _phoneHeaderSortUp = true;
	private Boolean _emailHeaderSortUp = true;
	//Book contains our contacts data set
	Book book = new Book();
	private JFrame popup = new JFrame();
	private final String VIEW_PANEL = "view";
	private final String ADD_PANEL = "add";
	private final String EDIT_PANEL = "edit";
	private CardLayout cards = new CardLayout();
	private JPanel panel_container = new JPanel();
	private ArrayList<JTextPane> panes = new ArrayList<>();

	//this is used to determine which row the user clicked on (selected) last
	private Integer selectedRow = 0;

	/*****************************************************************************************************
	 Create the application.
	 *****************************************************************************************************/
	ContactsBook() {
		initialize();
	}

	/*****************************************************************************************************
	 Initialize the contents of the frame.
	 *****************************************************************************************************/
	private void initialize() {
		InitializeFrame();
		InitializePanel();
		InitializeTable();
		InitializeLabels();
		InitializeTextFields();
		InitializeButtons();
		InitializeActionListeners();
		renderContactFormModal();
		initializeFieldEventHandlers();
	}

	/*****************************************************************************************************
	 Here we initialize our frame
	 *****************************************************************************************************/
	private void InitializeFrame() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.PINK);
		frame.setBackground(Color.PINK);
		frame.setBounds(100, 100, 1056, 650);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
	}

	/*****************************************************************************************************
	 Here we initialize our JTable, set its values and give it a default table model
	 *****************************************************************************************************/
	private void InitializeTable() {

		frame.getContentPane().add(tableScrollPanel);

		table = new JTable();
		table.setAutoCreateRowSorter(false);
		table.setCellSelectionEnabled(true);
		table.setColumnSelectionAllowed(false);
		DefaultTableModel defaultTableModel = new DefaultTableModel(){
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		defaultTableModel.addColumn("First");
		defaultTableModel.addColumn("Last");
		defaultTableModel.addColumn("Address Line 1");
		defaultTableModel.addColumn("Address Line 2");
		defaultTableModel.addColumn("City");
		defaultTableModel.addColumn("State");
		defaultTableModel.addColumn("Zipcode");
		defaultTableModel.addColumn("Phone");
		defaultTableModel.addColumn("E-mail");
		table.setModel(defaultTableModel);

		//When user clicks table, show modal with that user's information
		table.addMouseListener(new ViewContactTriggerModalEventHandler());

		tableScrollPanel.setViewportView(table);
		ListSelectionModel model = table.getSelectionModel();

	}
	/*****************************************************************************************************
	 Here we initialize our panels we use in the application
	 *****************************************************************************************************/
	private void InitializePanel() {

		contactNameAndAddContactScrollPanel = new JScrollPane();
		contactNameAndAddContactScrollPanel.setViewportBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		contactNameAndAddContactScrollPanel.setBounds(10, 25, 1010, 100);

		tableScrollPanel = new JScrollPane();
		tableScrollPanel.setViewportBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		tableScrollPanel.setBounds(10, 55, 1020, 536);

		contactsBookFinalLayout = new JPanel(new BorderLayout());
		contactsBookFinalLayout.add(contactNameAndAddContactScrollPanel);

	}
	/*****************************************************************************************************
	 Here we initialize our book title field
	 *****************************************************************************************************/
	private void InitializeLabels() {
		bookTitleField = new JTextPane();
		bookTitleField.setContentType("text/html");
		bookTitleField.setText(book.getBookName());
		bookTitleField.setBounds(12, 10, 400, 45);
		bookTitleField.setBackground(null);
		bookTitleField.setBorder(null);

		//Save book title when user edits the field
		bookTitleField.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				String bookTitle = book.getBookName();
				book.setBookName(bookTitle);
			}
			@Override
			public void removeUpdate(DocumentEvent e) {
				String bookTitle = book.getBookName();
				book.setBookName(bookTitle);
			}
			@Override
			public void changedUpdate(DocumentEvent e) {}
		});
		contactNameAndAddContactScrollPanel.add(bookTitleField);
		frame.add(bookTitleField);

		label_error = new JLabel("");
		label_error.setBounds(10, 400, 425, 100);label_error_edit = new JLabel("");
		label_error_edit.setBounds(10, 400, 425, 100);
	}

	/*****************************************************************************************************
	 Here we take in a string with some css and make an html wrapper around it and return that as a string
	 *****************************************************************************************************/
	private String writeHtmlForBody(String body, String css){
		return "<html><body><div style='" + css + "'>" + body + "</div></body></html>";
	}

	/*****************************************************************************************************
	 Here we create our text fields for our add contact and edit contact panels.  We pass in field length
	 validators to force the max length
	 *****************************************************************************************************/
	private void InitializeTextFields() {
		firstname_field = new JTextPane(new FieldLengthValidators.FirstNameLengthValidator());
		lastname_field = new JTextPane(new FieldLengthValidators.LastNameLengthValidator());
		address_line1_field = new JTextPane(new FieldLengthValidators.AddressLine1LengthValidator());
		address_line2_field = new JTextPane(new FieldLengthValidators.AddressLine2LengthValidator());
		city_field = new JTextPane(new FieldLengthValidators.CityNameLengthValidator());
		state_field = new JTextPane(new FieldLengthValidators.StateLengthValidator());
		zipcode_field = new JTextPane(new FieldLengthValidators.ZipcodeLengthValidator());
		phone_field = new JTextPane(new FieldLengthValidators.PhoneNumberLengthValidator());
		email_field = new JTextPane(new FieldLengthValidators.EmailValidator());
		facebook_field = new JTextPane();
		instagram_field = new JTextPane();
		twitter_field = new JTextPane();


		firstname_field_edit = new JTextPane(new FieldLengthValidators.FirstNameLengthValidator());
		lastname_field_edit = new JTextPane(new FieldLengthValidators.LastNameLengthValidator());
		address_line1_field_edit = new JTextPane(new FieldLengthValidators.AddressLine1LengthValidator());
		address_line2_field_edit = new JTextPane(new FieldLengthValidators.AddressLine2LengthValidator());
		city_field_edit = new JTextPane(new FieldLengthValidators.CityNameLengthValidator());
		state_field_edit = new JTextPane(new FieldLengthValidators.StateLengthValidator());
		zipcode_field_edit = new JTextPane(new FieldLengthValidators.ZipcodeLengthValidator());
		phone_field_edit = new JTextPane(new FieldLengthValidators.PhoneNumberLengthValidator());
		email_field_edit = new JTextPane(new FieldLengthValidators.EmailValidator());
		facebook_field_edit = new JTextPane();
		instagram_field_edit = new JTextPane();
		twitter_field_edit = new JTextPane();

		firstname_view_pane = new JTextPane(); panes.add(firstname_view_pane);
		lastname_view_pane = new JTextPane(); panes.add(lastname_view_pane);
		address_line1_view_pane = new JTextPane(); panes.add(address_line1_view_pane);
		address_line2_view_pane = new JTextPane(); panes.add(address_line2_view_pane);
		city_view_pane = new JTextPane(); panes.add(city_view_pane);
		state_view_pane = new JTextPane(); panes.add(state_view_pane);
		zipcode_view_pane = new JTextPane(); panes.add(zipcode_view_pane);
		phone_view_pane = new JTextPane(); panes.add(phone_view_pane);
		email_view_pane = new JTextPane(); panes.add(email_view_pane);
		facebook_view_pane = new JTextPane(); panes.add(facebook_view_pane);

		instagram_view_pane = new JTextPane(); panes.add(instagram_view_pane);
		twitter_view_pane = new JTextPane(); panes.add(twitter_view_pane);

		//Initialize all panes with these properties
		for(JTextPane pane : panes){
			pane.setContentType("text/html");
			pane.setBackground(null);
			pane.setBorder(null);
			pane.setEditable(false);
			pane.setText("");
		}
		ImageIcon facebookIcon = new ImageIcon(getClass().getResource("facebooklogo.png"));
		facebook_view_pane.insertIcon(facebookIcon);
		ImageIcon twitterLogoIcon = new ImageIcon(getClass().getResource("twitterlogo.png"));
		twitter_view_pane.insertIcon(twitterLogoIcon);
		ImageIcon instagramIcon = new ImageIcon(getClass().getResource("instagramlogo.png"));
		instagram_view_pane.insertIcon(instagramIcon);
	}
	/*****************************************************************************************************
	 Here we create our edit contact panel and add the various fields to the panel.  These fields include
	 the various contact attributes which will be editable and saveable from within the popup modal
	 *****************************************************************************************************/
	private void InitializeButtons() {
		//############################################################
		//  BUTTONS USED IN MAIN WINDOW
		//############################################################

		add_contact_button = new JButton("+");
		add_contact_button.setBounds(980, 9, 45, 23);
		contactNameAndAddContactScrollPanel.add(add_contact_button);
		frame.getContentPane().add(add_contact_button);

		//############################################################
		//  BUTTONS USED IN POPUP DIALOGUE
		//############################################################
		create_button = new JButton("Create");
		create_button.addActionListener(new CreateContactEventHandler());

		edit_button = new JButton("Edit Contact");
		edit_button.addActionListener(new EditContactTriggerModalEventHandler());

		save_button = new JButton("Save");
		save_button.addActionListener(new SaveContactEventHandler());

		cancel_button = new JButton("Cancel");
		cancel_button.addActionListener(new CancelCreateContactEventHandler());


	}
	/*****************************************************************************************************
	 A method that wires up the event handler for the add contact modal that causes the add panel to
	 appear within the popup modal
	 *****************************************************************************************************/
	private void InitializeActionListeners() {
		add_contact_button.addMouseListener(new AddContactTriggerModalEventHandler());
		JTableHeader header = table.getTableHeader();
		header.addMouseListener(new ContactsBookHeaderEventHandler());
	}
	/*****************************************************************************************************
	 Here we wire up our event handlers for our various textPanels.  These panels listen on text change
	 upon which validation occurs
	 *****************************************************************************************************/
	private boolean initializeFieldEventHandlers(){

		firstname_field.getDocument().addDocumentListener(new PopupFieldAlteredEventHandler());
		lastname_field.getDocument().addDocumentListener(new PopupFieldAlteredEventHandler());
		address_line1_field.getDocument().addDocumentListener(new PopupFieldAlteredEventHandler());
		address_line2_field.getDocument().addDocumentListener(new PopupFieldAlteredEventHandler());
		city_field.getDocument().addDocumentListener(new PopupFieldAlteredEventHandler());
		state_field.getDocument().addDocumentListener(new PopupFieldAlteredEventHandler());
		zipcode_field.getDocument().addDocumentListener(new PopupFieldAlteredEventHandler());
		phone_field.getDocument().addDocumentListener(new PopupFieldAlteredEventHandler());
		email_field.getDocument().addDocumentListener(new PopupFieldAlteredEventHandler());
		facebook_field.getDocument().addDocumentListener(new PopupFieldAlteredEventHandler());
		twitter_field.getDocument().addDocumentListener(new PopupFieldAlteredEventHandler());
		instagram_field.getDocument().addDocumentListener(new PopupFieldAlteredEventHandler());

		firstname_field_edit.getDocument().addDocumentListener(new PopupFieldEditAlteredEventHandler());
		lastname_field_edit.getDocument().addDocumentListener(new PopupFieldEditAlteredEventHandler());
		address_line1_field_edit.getDocument().addDocumentListener(new PopupFieldEditAlteredEventHandler());
		address_line2_field_edit.getDocument().addDocumentListener(new PopupFieldEditAlteredEventHandler());
		city_field_edit.getDocument().addDocumentListener(new PopupFieldEditAlteredEventHandler());
		state_field_edit.getDocument().addDocumentListener(new PopupFieldEditAlteredEventHandler());
		zipcode_field_edit.getDocument().addDocumentListener(new PopupFieldEditAlteredEventHandler());
		phone_field_edit.getDocument().addDocumentListener(new PopupFieldEditAlteredEventHandler());
		email_field_edit.getDocument().addDocumentListener(new PopupFieldEditAlteredEventHandler());
		facebook_field_edit.getDocument().addDocumentListener(new PopupFieldEditAlteredEventHandler());
		twitter_field_edit.getDocument().addDocumentListener(new PopupFieldEditAlteredEventHandler());
		instagram_field_edit.getDocument().addDocumentListener(new PopupFieldEditAlteredEventHandler());
		return true;
	}
	/*****************************************************************************************************
	 Here we perform a validation with all of the validation criteria and set an error that appears at
	 the bottom of the popup modal.  The boolean return will disallow saves, or adds
	 *****************************************************************************************************/
	private boolean checkFieldsForValidation(String firstName, String lastName, String addressLine1, String addressLine2, String city, String state, String zipCode, String phone, String email, String facebook, String twitter, String instagram){
		//first we check to see if there is a first name and/or last name
		Boolean firstLastValidated = validateFirstNameLastName(firstName, lastName);
		//we validate our other fields
		java.util.List<String> otherFieldsValidated = validateZipcodePhoneNumberAndEmail(zipCode, phone, email);
		//if first or last was invalid we give error and have the user retry
		if(!firstLastValidated){
			otherFieldsValidated.add("You need either a first name or a last name.");
			String errorMessage = getJLabelErrorString(otherFieldsValidated);
			label_error.setText(errorMessage);
			label_error_edit.setText(errorMessage);
			return false;
		}
		//the contact has to have one field other than a name field
		Integer check = checkHowManyFieldsHaveValues(addressLine1, addressLine2, city, state, zipCode, phone, email, facebook, twitter, instagram);
		if(check < 1){
			otherFieldsValidated.add("You need at least one other field to go with your first/last name");
			String errorMessage = getJLabelErrorString(otherFieldsValidated);
			label_error.setText(errorMessage);
			label_error_edit.setText(errorMessage);
			return false;
		}
		String errorMessage = getJLabelErrorString(otherFieldsValidated);
		//set the error messages
		label_error.setText(errorMessage);
		label_error_edit.setText(errorMessage);

		return true;
	}
	/*****************************************************************************************************
	 Here we clear the text fields for our popup modal
	 *****************************************************************************************************/
	private void clearTextFields(){
		firstname_field.setText("");
		lastname_field.setText("");
		address_line1_field.setText("");
		address_line2_field.setText("");
		city_field.setText("");
		state_field.setText("");
		zipcode_field.setText("");
		phone_field.setText("");
		email_field.setText("");
		facebook_field.setText("");
		instagram_field.setText("");
		twitter_field.setText("");
		label_error.setText("");
		label_error_edit.setText("");
	}
	/*****************************************************************************************************
	 Here we load the JTable with te Object[] list passed in
	 *****************************************************************************************************/
	void loadJTableWithData(ArrayList<Object[]> contactsBookList){
		Object[][] contactsBook = contactsBookList.toArray(new Object[][]{});

		DefaultTableModel model = (DefaultTableModel) table.getModel();
		//set columns
		String[] columns = new String[]{
				"First", "Last", "Address Line 1", "Address Line 2", "City", "State", "Zipcode", "Phone", "E-mail"
		};
		//load the data into the table
		model.setDataVector(contactsBook, columns);
	}
	private Boolean validateFirstNameLastName(String firstName, String lastName){
		return !(firstName.equals("") && lastName.equals(""));
	}
	/*****************************************************************************************************
	 Here we use regular expressions to determine if we have a valid zipcode, phone number, and email.
	 If not error messages are loaded which show up in the bottom of the popup modal
	 *****************************************************************************************************/
	private java.util.List<String> validateZipcodePhoneNumberAndEmail(String zipcode, String phoneNumber, String email){
		java.util.List<String> errorList = new ArrayList<String>();
		//First we validate and make sure zipcode is valid
		String regex = "^[0-9]{5}(?:-[0-9]{4})?$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(zipcode);
		if(!matcher.matches() && !zipcode.equals("")){
			errorList.add("Zipcode does not meet U.S. postal code standards");
		}
		//validate the phone number
		regex = "^\\(?([0-9]{3})\\)?[-.\\s]?([0-9]{3})[-.\\s]?([0-9]{4})$";
		pattern = Pattern.compile(regex);
		matcher = pattern.matcher(phoneNumber);
		if(!matcher.matches() && !phoneNumber.equals("")){
			errorList.add("Phone number does not meet standard U.S. phone number format");
		}
		//Now we validate the email address
		regex = "^[A-Za-z0-9+_.-]+@(.+)$";
		pattern = Pattern.compile(regex);
		matcher = pattern.matcher(email);
		if(!matcher.matches() && !email.equals("")){
			errorList.add("Email is not in proper format");
		}
		return errorList;
	}
	/*****************************************************************************************************
	 We use this to help with validation and that we have to have at least one other non name field in
	 order for the contact's attributes to be valid.  We iterate over each of the attributes passed in
	 and count the ones that are not empty
	 *****************************************************************************************************/
	private Integer checkHowManyFieldsHaveValues(String addressLine1, String addresLine2, String city, String state, String zipcode,  String phone, String email, String facebook, String twitter, String instagram){
		Integer numberHaveValues = 0;
		if(!addressLine1.equals("")){
			numberHaveValues++;
		}
		if(!addresLine2.equals("")){
			numberHaveValues++;
		}
		if(!city.equals("")){
			numberHaveValues++;
		}
		if(!state.equals("")){
			numberHaveValues++;
		}
		if(!zipcode.equals("")){
			numberHaveValues++;
		}
		if(!phone.equals("")){
			numberHaveValues++;
		}
		if(!email.equals("")){
			numberHaveValues++;
		}
		if(!facebook.equals("")){
			numberHaveValues++;
		}
		if(!twitter.equals("")){
			numberHaveValues++;
		}
		if(!instagram.equals("")){
			numberHaveValues++;
		}
		return numberHaveValues;
	}
	/*****************************************************************************************************
	 Here we initialize our contact popup modal for later use
	 *****************************************************************************************************/
	private void renderContactFormModal(){

		//Create modal
		//popup.setTitle("Create Contact");
		popup.setBounds(350, 300, 450, 525);
		popup.setAlwaysOnTop(false);
		popup.getContentPane().setBackground(Color.PINK);
		popup.setResizable(false);

		//Card layout allows for switching of JPanels
		panel_container.setLayout(cards /*New card Layout*/);

		//Create each layer of usable JPanels
		JPanel viewContactPanel = renderViewContactPanel();
		JPanel addContactPanel = renderAddContactPanel();
		JPanel editContactPanel = renderEditContactPanel();

		//Add each panel to popup; they will be shown and hidden programmatically
		panel_container.add(viewContactPanel, VIEW_PANEL);
		panel_container.add(addContactPanel, ADD_PANEL);
		panel_container.add(editContactPanel, EDIT_PANEL);

		//Show view panel on default
		cards.show(panel_container, VIEW_PANEL);
		popup.add(panel_container);
	}
	/*****************************************************************************************************
	 Here we create our contact view panel and add the various fields to the panel.  These fields include
	 the various contact attributes which will be viewable from within the popup modal
	 *****************************************************************************************************/
	private JPanel renderViewContactPanel(){
		JPanel view_panel = new JPanel();
		view_panel.setLayout(null);

		//Create labels
		JLabel label_first_name = new JLabel("First Name");
		JLabel label_last_name = new JLabel("Last Name");
		JLabel label_address_line1 = new JLabel("Address line 1");
		JLabel label_address_line2 = new JLabel("Address line 2");
		JLabel label_city = new JLabel("City");
		JLabel label_state = new JLabel("State");
		JLabel label_zipcode = new JLabel("Zipcode");
		JLabel label_phone = new JLabel("Phone");
		JLabel label_email = new JLabel("E-mail");
		int nameLine = 11;
		int addressLine = 70;
		int cityStateZipLine = 130;
		int phoneEmailLine = 190;
		int facebookLine = 250;

		//Set label locations
		label_first_name.setBounds(10, nameLine, 128, 14);
		label_last_name.setBounds(225, nameLine, 128, 14);

		label_address_line1.setBounds(10, addressLine, 188, 14);
		label_address_line2.setBounds(225, addressLine, 188, 14);

		label_city.setBounds(10, cityStateZipLine, 46, 14);
		label_state.setBounds(112, cityStateZipLine, 46, 14);
		label_zipcode.setBounds(225, cityStateZipLine, 128, 14);

		label_phone.setBounds(10, phoneEmailLine, 46, 14);
		label_email.setBounds(225, phoneEmailLine, 46, 14);

		//Add labels to the panel
		view_panel.add(label_first_name);
		view_panel.add(label_last_name);
		view_panel.add(label_address_line1);
		view_panel.add(label_address_line2);
		view_panel.add(label_city);
		view_panel.add(label_state);
		view_panel.add(label_zipcode);
		view_panel.add(label_phone);
		view_panel.add(label_email);

		//Set bounds for each label
		int col_one_start = 10;
		int col_two_start = 225;
		int col_one_end = col_two_start-20;
		firstname_view_pane.setBounds(col_one_start, 36, col_one_end, 20);
		lastname_view_pane.setBounds(col_two_start, 36, col_one_end, 20);
		address_line1_view_pane.setBounds(col_one_start, 92, col_one_end, 20);
		address_line2_view_pane.setBounds(col_two_start, 92, col_one_end, 20);
		city_view_pane.setBounds(col_one_start, 150, 86, 20);
		state_view_pane.setBounds(112, 150, 93, 20);
		zipcode_view_pane.setBounds(col_two_start, 150, 86, 20);
		phone_view_pane.setBounds(col_one_start, 210, col_one_end, 20);
		email_view_pane.setBounds(col_two_start, 210, col_one_end, 20);
		facebook_view_pane.setBounds(col_one_start, facebookLine+20, col_one_end, 200);
		instagram_view_pane.setBounds(col_one_start, facebookLine+120+20, col_one_end, 200);
		twitter_view_pane.setBounds(col_one_start, facebookLine+60+20, col_one_end, 200);
		facebookJLabel = new JLabel();
		facebook_view_pane.addMouseListener(new FacebookLinkClickedEventHandler());
		twitter_view_pane.addMouseListener(new TwitterLinkClickedEventHandler());
		instagram_view_pane.addMouseListener(new InstagramLinkClickedEventHandler());

		//Add all fields to frame
		view_panel.add(firstname_view_pane);
		view_panel.add(lastname_view_pane);
		view_panel.add(address_line1_view_pane);
		view_panel.add(address_line2_view_pane);
		view_panel.add(city_view_pane);
		view_panel.add(state_view_pane);
		view_panel.add(zipcode_view_pane);
		view_panel.add(phone_view_pane);
		view_panel.add(email_view_pane);
		view_panel.add(facebook_view_pane);
		view_panel.add(instagram_view_pane);
		view_panel.add(twitter_view_pane);
		view_panel.add(facebookJLabel);
		view_panel.add(label_error);
		view_panel.add(label_error_edit);

		//clear text fields if the popup window is closed
		popup.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				clearTextFields();
				frame.setEnabled(true);
			}
		});
		//set boundaries for the edit and cancel buttons
		edit_button.setBounds(col_two_start+40, 300, 125, 20);
		cancel_button.setBounds(col_two_start+40, 360, 125, 20);
		edit_button.setVisible(true);
		cancel_button.setVisible(true);
		//set boundaries for the delete contact button
		delete_contact_button = new JButton("Delete Contact");
		delete_contact_button.setBounds(col_two_start+40, 330, 125, 20);
		delete_contact_button.addMouseListener(new DeleteContactEventHandler());

		//add the buttons to the view panel
		view_panel.add(delete_contact_button);
		view_panel.add(edit_button);
		view_panel.add(cancel_button);
		view_panel.setVisible(true);
		return view_panel;
	}
	/*****************************************************************************************************
	 Here we create our add contact panel and add the various fields to the panel.  These fields include
	 the various contact attributes which will be added and will be saveable from within the popup modal
	 *****************************************************************************************************/
	private JPanel renderAddContactPanel(){
		JPanel add_panel = new JPanel();
		add_panel.setLayout(null);

		//Create labels
		JLabel label_first_name = new JLabel("First Name");
		JLabel label_last_name = new JLabel("Last Name");
		JLabel label_address_line1 = new JLabel("Address line 1");
		JLabel label_address_line2 = new JLabel("Address line 2");
		JLabel label_city = new JLabel("City");
		JLabel label_state = new JLabel("State");
		JLabel label_zipcode = new JLabel("Zipcode");
		JLabel label_phone = new JLabel("Phone");
		JLabel label_email = new JLabel("E-mail");
		JLabel label_facebook = new JLabel("Facebook");
		JLabel label_instagram = new JLabel("Instagram");
		JLabel label_twitter = new JLabel("Twitter");

		int nameLine = 10;
		int addressLine = 70;
		int cityStateZipLine = 130;
		int phoneEmailLine = 190;
		int facebookLine = 250;

		//Set label locations
		label_first_name.setBounds(10, nameLine, 128, 14);
		label_last_name.setBounds(225, nameLine, 128, 14);
		label_address_line1.setBounds(10, addressLine, 188, 14);
		label_address_line2.setBounds(225, addressLine, 188, 14);
		label_city.setBounds(10, cityStateZipLine, 46, 14);
		label_state.setBounds(112, cityStateZipLine, 46, 14);
		label_zipcode.setBounds(225, cityStateZipLine, 128, 14);
		label_phone.setBounds(10, phoneEmailLine, 46, 14);
		label_email.setBounds(225, phoneEmailLine, 46, 14);

		label_facebook.setBounds(10, facebookLine, 128, 14);
		label_instagram.setBounds(10, facebookLine+60, 128, 14);
		label_twitter.setBounds(10, facebookLine+120, 128, 14);

		//Add labels to the panel
		add_panel.add(label_first_name);
		add_panel.add(label_last_name);
		add_panel.add(label_address_line1);
		add_panel.add(label_address_line2);
		add_panel.add(label_city);
		add_panel.add(label_state);
		add_panel.add(label_zipcode);
		add_panel.add(label_phone);
		add_panel.add(label_email);
		add_panel.add(label_facebook);
		add_panel.add(label_instagram);
		add_panel.add(label_twitter);

		//Set bounds for each label
		int col_one_start = 10;
		int col_two_start = 225;
		int col_one_end = col_two_start-20;
		firstname_field.setBounds(col_one_start, 36, col_one_end, 20);
		lastname_field.setBounds(col_two_start, 36, col_one_end, 20);
		address_line1_field.setBounds(col_one_start, 92, col_one_end, 20);
		address_line2_field.setBounds(col_two_start, 92, col_one_end, 20);
		city_field.setBounds(col_one_start, 150, 86, 20);
		state_field.setBounds(112, 150, 93, 20);
		zipcode_field.setBounds(col_two_start, 150, 86, 20);
		phone_field.setBounds(col_one_start, 210, col_one_end, 20);
		email_field.setBounds(col_two_start, 210, col_one_end, 20);

		facebook_field.setBounds(col_one_start, facebookLine+20, col_one_end, 20);
		instagram_field.setBounds(col_one_start, facebookLine+60+20, col_one_end, 20);
		twitter_field.setBounds(col_one_start, facebookLine+120+20, col_one_end, 20);

		ArrayList<JTextPane> fields = new ArrayList<>();
		fields.add(firstname_field); fields.add(lastname_field); fields.add(address_line1_field); fields.add(address_line2_field);
		fields.add(city_field); fields.add(state_field);fields.add(zipcode_field);fields.add(phone_field);fields.add(email_field);
		fields.add(facebook_field); fields.add(instagram_field); fields.add(twitter_field);

		//Shortens code into loop
		for(JTextPane f : fields){
			add_panel.add(f);
		}
		add_panel.add(label_error);
		//if the  window is closed clear the text fields for the next contact to be loaded
		popup.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				clearTextFields();
				frame.setEnabled(true);
			}
		});
		//set cancel and add buttons boundaries
		create_button.setBounds(col_two_start+40, 300, 125, 20);
		cancel_button.setBounds(col_two_start+40, 360, 125, 20);
		add_panel.add(create_button);
		//add_panel.add(cancel_button);

		//popup.add(view_panel);
		add_panel.setVisible(true);
		return add_panel;
	}
	/*****************************************************************************************************
	 Here we create our edit contact panel and add the various fields to the panel.  These fields include
	 the various contact attributes which will be editable and saveable from within the popup modal
	 *****************************************************************************************************/
	private JPanel renderEditContactPanel(){

		JPanel edit_panel = new JPanel();
		edit_panel.setLayout(null);

		//Create labels
		JLabel label_first_name = new JLabel("First Name");
		JLabel label_last_name = new JLabel("Last Name");
		JLabel label_address_line1 = new JLabel("Address line 1");
		JLabel label_address_line2 = new JLabel("Address line 2");
		JLabel label_city = new JLabel("City");
		JLabel label_state = new JLabel("State");
		JLabel label_zipcode = new JLabel("Zipcode");
		JLabel label_phone = new JLabel("Phone");
		JLabel label_email = new JLabel("E-mail");
		JLabel label_facebook = new JLabel("Facebook");
		JLabel label_instagram = new JLabel("Instagram");
		JLabel label_twitter = new JLabel("Twitter");

		int nameLine = 10;
		int addressLine = 70;
		int cityStateZipLine = 130;
		int phoneEmailLine = 190;
		int facebookLine = 250;

		//Set label locations
		label_first_name.setBounds(10, nameLine, 128, 14);
		label_last_name.setBounds(225, nameLine, 128, 14);
		label_address_line1.setBounds(10, addressLine, 188, 14);
		label_address_line2.setBounds(225, addressLine, 188, 14);
		label_city.setBounds(10, cityStateZipLine, 46, 14);
		label_state.setBounds(112, cityStateZipLine, 46, 14);
		label_zipcode.setBounds(225, cityStateZipLine, 128, 14);
		label_phone.setBounds(10, phoneEmailLine, 46, 14);
		label_email.setBounds(225, phoneEmailLine, 46, 14);

		label_facebook.setBounds(10, facebookLine, 128, 14);
		label_instagram.setBounds(10, facebookLine+60, 128, 14);
		label_twitter.setBounds(10, facebookLine+120, 128, 14);

		//Add labels to the panel
		edit_panel.add(label_first_name);
		edit_panel.add(label_last_name);
		edit_panel.add(label_address_line1);
		edit_panel.add(label_address_line2);
		edit_panel.add(label_city);
		edit_panel.add(label_state);
		edit_panel.add(label_zipcode);
		edit_panel.add(label_phone);
		edit_panel.add(label_email);
		edit_panel.add(label_facebook);
		edit_panel.add(label_instagram);
		edit_panel.add(label_twitter);

		//Set bounds for each label
		int col_one_start = 10;
		int col_two_start = 225;
		int col_one_end = col_two_start-20;
		firstname_field_edit.setBounds(col_one_start, 36, col_one_end, 20);
		lastname_field_edit.setBounds(col_two_start, 36, col_one_end, 20);
		address_line1_field_edit.setBounds(col_one_start, 92, col_one_end, 20);
		address_line2_field_edit.setBounds(col_two_start, 92, col_one_end, 20);
		city_field_edit.setBounds(col_one_start, 150, 86, 20);
		state_field_edit.setBounds(112, 150, 93, 20);
		zipcode_field_edit.setBounds(col_two_start, 150, 86, 20);
		phone_field_edit.setBounds(col_one_start, 210, col_one_end, 20);
		email_field_edit.setBounds(col_two_start, 210, col_one_end, 20);

		facebook_field_edit.setBounds(col_one_start, facebookLine+20, col_one_end, 20);
		instagram_field_edit.setBounds(col_one_start, facebookLine+60+20, col_one_end, 20);
		twitter_field_edit.setBounds(col_one_start, facebookLine+120+20, col_one_end, 20);

		ArrayList<JTextPane> fields = new ArrayList<>();
		fields.add(lastname_field_edit); fields.add(address_line1_field_edit); fields.add(address_line2_field_edit);
		fields.add(city_field_edit); fields.add(state_field_edit);fields.add(zipcode_field_edit);fields.add(phone_field_edit);fields.add(email_field_edit);
		fields.add(facebook_field_edit); fields.add(instagram_field_edit); fields.add(twitter_field_edit);

		//Shortens code into loop
		for(JTextPane f : fields){
			edit_panel.add(f);
		}
		edit_panel.add(firstname_field_edit);
		edit_panel.add(label_error_edit);

		popup.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				clearTextFields();
				frame.setEnabled(true);
			}
		});
		//set boundaries for the save and cancel button for the contact edit panel
		save_button.setBounds(col_two_start+40, 300, 125, 20);
		cancel_button.setBounds(col_two_start+40, 360, 125, 20);
		edit_panel.add(save_button);
		edit_panel.add(cancel_button);
		edit_panel.setVisible(true);
		return edit_panel;
	}
	/*****************************************************************************************************
	 Creates a popup and returns true if the user clicks yes
	 *****************************************************************************************************/
	boolean renderConfirmationModal(String frameTitle, String message){
		int response = JOptionPane.showConfirmDialog(null, message, frameTitle, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		//If the answer was yes, return true
		return (response == JOptionPane.YES_OPTION);
	}

	/*****************************************************************************************************
	 Creates popup with errorMessage as text. Warns the user
	 *****************************************************************************************************/
	void renderErrorMessageModal(String errorMessage){
		JFrame parent = new JFrame();
		JOptionPane.showMessageDialog(parent, errorMessage);
	}
	/*****************************************************************************************************
	 this gets, for the sort type, if the sort should be ascending or descending
	 *****************************************************************************************************/
	private Boolean getSortForSortingTypeForEditCreateContact(SortingType sortType){

		switch (sortType) {
			case FIRSTNAME:
				//then we are going to sort by first name
				return !_firstNameHeaderSortUp;
			case LASTNAME:
				//then we are going to sort by last name
				return !_lastNameHeaderSortUp;
			case ADDRESSLINE1:
				//then we are going to sort by AddressLine1
				return !_addressLine1HeaderSortUp;
			case ADDRESSLINE2:
				//then we are going to sort by AddressLine2
				return !_addressLine2HeaderSortUp;
			case CITY:
				//then we are going to sort by city
				return !_cityHeaderSortUp;
			case STATE:
				//then we are going to sort by state
				return !_stateHeaderSortUp;
			case ZIPCODE:
				//else we are going to sort by zipcode
				return !_zipcodeHeaderSortUp;
			case PHONE:
				//then we are going to sort by phone
				return !_phoneHeaderSortUp;
			case EMAIL:
				//then we are going to sort by email
				return !_emailHeaderSortUp;
			default:
				//then we are going to sort by last name
				return !_lastNameHeaderSortUp;
		}
	}
	/*****************************************************************************************************
	 This is the method that is called when an error has occured during validation. The method creates an
	 html string of the various errors. Used in the contact view and edit popup modal
	 *****************************************************************************************************/
	private String getJLabelErrorString(java.util.List<String> errorList){
		if(errorList.size() == 0){
			return "";
		}
		String message = "<html><body><ul>";
		for(String error: errorList){
			message+= "<li>" + error + "</li>";
		}
		message+="</ul></body></html>";
		return message;
	}
	String chooseFilePathAndNameForSave(String name, Boolean useFileNameAndPath) {

		if(book.getBookFileNameAndPath() != null && !book.getBookFileNameAndPath().equals("") && useFileNameAndPath){
			return book.getBookFileNameAndPath();
		}
		else {

			try {
				String contactsBookName = book.getBookName();
				if (!name.equals("")) {
					contactsBookName = name;
				}
				else if (contactsBookName.equals("")) {
					contactsBookName = "mycontactsbook";
				}
				contactsBookName = contactsBookName.replaceAll("\\s+", "");
				Random r = new Random();
				int randomInteger = r.nextInt(10000);
				contactsBookName += "_" + randomInteger + ".txt";
				if(book.getBookFileNameAndPath() != null && !book.getBookFileNameAndPath().equals("")){
					contactsBookName = book.getBookFileNameAndPath();
				}

				File file = new File(contactsBookName);
				JFileChooser fc = new JFileChooser();
				fc.setSelectedFile(file);
				Integer filePathFound  = fc.showSaveDialog(ContactsBook.this.frame);
				if(filePathFound == JFileChooser.CANCEL_OPTION){
					return null;
				}
				if(filePathFound.equals(JFileChooser.ERROR_OPTION)){
					return null;
				}
				File selectedFile = fc.getSelectedFile();
				return selectedFile.toString();
			} catch (Exception e) {
				return null;
			}
		}
	}
	/*****************************************************************************************************
	 This is the method that is called that loads the contactsBook of contacts with the data from the
	 JTable
	 *****************************************************************************************************/
	void setContactsBookFromJTableModel(){
		DefaultTableModel defaultTableModel = (DefaultTableModel) table.getModel();
		int numberOfRows = defaultTableModel.getRowCount();
		int numberOfColumns = defaultTableModel.getColumnCount();
		java.util.ArrayList<Object[]> contactsBook = new ArrayList<Object[]>();
		for(int i = 0; i < numberOfRows; i++){
			//create our row of contact attributes
			Object[] currentRow = new Object[numberOfColumns+3];
			for(int j = 0; j < numberOfColumns; j++){
				currentRow[j] = defaultTableModel.getValueAt(i,j);
			}
			//need to also set the social media fields manually
			Object[] currentRowFromModel = book.getContactsBook().get(i);
			currentRow[9] = currentRowFromModel[9];
			currentRow[10] = currentRowFromModel[10];
			currentRow[11] = currentRowFromModel[11];
			contactsBook.add(currentRow);
		}
		//add the new set of contacts to our book member variable
		book.setContactsBook(contactsBook);
	}
	/*****************************************************************************************************
	 This is the method that is called that sets the book title and saves the book
	 *****************************************************************************************************/
	void setBookNamePathAndSave() throws IOException{
		String pathAndFileName = chooseFilePathAndNameForSave("", true);
		if(pathAndFileName == null){
			return;
		}

		book.setBookFileNameAndPath(pathAndFileName);
		book.saveBook();
	}
	/*****************************************************************************************************
	 This is the method that is called when the user searches for with a particular search term
	 *****************************************************************************************************/
	void search(String searchTerm) {
		//search through all of book.contactBook
		_searchTerm = searchTerm;
		if(searchTerm.equals("")){
			loadJTableWithData(book.getContactsBook());
			return;
		}
		//search by the search term.
		book.search(searchTerm);
		ArrayList<Object[]> sortedList = book.getSearchedContactsBook();
		//load the search results into the JTable
		loadJTableWithData(sortedList);
	}
	/*****************************************************************************************************
	 This is the method that is called when we want to show our popup modal which holds our contact view,
	 contact edit panes.
	 *****************************************************************************************************/
	void addContactTriggerModal() {

		//Disable main window
		frame.setEnabled(false);
		//Show add options for modal
		popup.setTitle("Create Contact");
		cards.show(panel_container, ADD_PANEL);
		//show modal
		popup.setVisible(true);

	}

	/*****************************************************************************************************
	 This is the method that is called when the Facebook image has been clicked.  The event handler tries
	 to open up a browser and load the Facebook profile of the contact
	 *****************************************************************************************************/
	private class FacebookLinkClickedEventHandler implements MouseListener{

		@Override
		public void mouseClicked(MouseEvent mouseEvent) {
			try {
				//get the url for this contact's Facebook account
				String url  = facebook_field_edit.getText();
				if(url.equals("")){
					int selectedRow = table.getSelectedRow();
					if(selectedRow != -1) {
						//Need to populate the modal fields with data
						Object[] lastRowClicked = book.getContactsBook().get(selectedRow);
						url = (String) lastRowClicked[ContactAttributeIndexer.FACEBOOK.getIndex()];

					}
				}
				//try to load a browser with the facebook url
				Desktop.getDesktop().browse(new URI(url));
			} catch (IOException e) {
				renderErrorMessageModal("There was a problem opening up a browser to load your social medi");
			} catch (URISyntaxException e) {
				renderErrorMessageModal("There was a problem with the url you saved for your Facebook contact.  Please verify that the url is a valid url.");
			}
		}
		@Override
		public void mousePressed(MouseEvent mouseEvent) {}
		@Override
		public void mouseReleased(MouseEvent mouseEvent) {}
		@Override
		public void mouseEntered(MouseEvent mouseEvent) {}
		@Override
		public void mouseExited(MouseEvent mouseEvent) {}
	}
	/*****************************************************************************************************
	 This is the method that is called when the twitter image has been clicked.  The event handler tries
	 to open up a browser and load the twitter profile of the contact
	 *****************************************************************************************************/
	private class TwitterLinkClickedEventHandler implements MouseListener{

		@Override
		public void mouseClicked(MouseEvent mouseEvent) {
			try {
				//get the url for the contact's twitter account
				String url  = twitter_field_edit.getText();
				if(url.equals("")){
					int selectedRow = table.getSelectedRow();
					if(selectedRow != -1) {
						//Need to populate the modal fields with data
						Object[] lastRowClicked = book.getContactsBook().get(selectedRow);
						url = (String) lastRowClicked[ContactAttributeIndexer.TWITTER.getIndex()];

					}
				}
				//try to open a browser
				Desktop.getDesktop().browse(new URI(url));
			} catch (IOException e) {
				renderErrorMessageModal("There was a problem opening up a browser to load your social media url.");
			} catch (URISyntaxException e) {
				renderErrorMessageModal("There was a problem with the url you saved for your Twitter contact.  Please verify that the url is a valid url.");
			}
		}
		@Override
		public void mousePressed(MouseEvent mouseEvent) {}
		@Override
		public void mouseReleased(MouseEvent mouseEvent) {}
		@Override
		public void mouseEntered(MouseEvent mouseEvent) {}
		@Override
		public void mouseExited(MouseEvent mouseEvent) {}
	}
	/*****************************************************************************************************
	 This is the method that is called when the instagram image has been clicked.  The event handler tries
	 to open up a browser and load the instagram profile of the contact
	 *****************************************************************************************************/
	private class InstagramLinkClickedEventHandler implements MouseListener{

		@Override
		public void mouseClicked(MouseEvent mouseEvent) {
			try {
				//get the url for the instagram contact
				String url  = instagram_field_edit.getText();
				if(url.equals("")){
					//get the current contact
					int selectedRow = table.getSelectedRow();
					if(selectedRow != -1) {
						//Need to populate the modal fields with data
						Object[] lastRowClicked = book.getContactsBook().get(selectedRow);
						url = (String) lastRowClicked[ContactAttributeIndexer.INSTAGRAM.getIndex()];
					}
				}

				Desktop.getDesktop().browse(new URI(url));
			} catch (IOException e) {
				renderErrorMessageModal("There was a problem opening up a browser to load your social media url.");
			} catch (URISyntaxException e) {
				renderErrorMessageModal("There was a problem with the url you saved for your Instagram contact.  Please verify that the url is a valid url.");
			}
		}
		@Override
		public void mousePressed(MouseEvent mouseEvent) {}
		@Override
		public void mouseReleased(MouseEvent mouseEvent) {}
		@Override
		public void mouseEntered(MouseEvent mouseEvent) {}
		@Override
		public void mouseExited(MouseEvent mouseEvent) {}
	}
	/*****************************************************************************************************
	 This is the method that is called when one of the text fields in the contact add atrributes popup modal has changed.
	 All of those JTextPane's, for contact add popup,subscribe to this event handler
	 *****************************************************************************************************/
	private class PopupFieldAlteredEventHandler implements DocumentListener{

		@Override
		public void insertUpdate(DocumentEvent documentEvent) {
			//we check the edit text fields for validity.  This allows violations to be printed in the error section that shows up at the bottom of the popup modal
			checkFieldsForValidation(firstname_field.getText(), lastname_field.getText(), address_line1_field.getText(), address_line2_field.getText(), city_field.getText(), state_field.getText(), zipcode_field.getText(), phone_field.getText(), email_field.getText(), facebook_field.getText(), twitter_field.getText(), instagram_field.getText());
		}

		@Override
		public void removeUpdate(DocumentEvent documentEvent) {
			//we check the edit text fields for validity.  This allows violations to be printed in the error section that shows up at the bottom of the popup modal
			checkFieldsForValidation(firstname_field.getText(), lastname_field.getText(), address_line1_field.getText(), address_line2_field.getText(), city_field.getText(), state_field.getText(), zipcode_field.getText(), phone_field.getText(), email_field.getText(), facebook_field.getText(), twitter_field.getText(), instagram_field.getText());
		}

		@Override
		public void changedUpdate(DocumentEvent documentEvent) {

		}
	}
	/*****************************************************************************************************
	 This is the method that is called when one of the text fields iin the contact edit atrributes popup modal has changed.
	 All of those JTextPane's, for contact edit popup,subscribe to this event handler
	 *****************************************************************************************************/
	private class PopupFieldEditAlteredEventHandler implements DocumentListener{

		@Override
		public void insertUpdate(DocumentEvent documentEvent) {
			//we check the edit text fields for validity.  This allows violations to be printed in the error section that shows up at the bottom of the popup modal
			checkFieldsForValidation(firstname_field_edit.getText(), lastname_field_edit.getText(), address_line1_field_edit.getText(), address_line2_field_edit.getText(), city_field_edit.getText(), state_field_edit.getText(), zipcode_field_edit.getText(), phone_field_edit.getText(), email_field_edit.getText(), facebook_field_edit.getText(), twitter_field_edit.getText(), instagram_field_edit.getText());
		}

		@Override
		public void removeUpdate(DocumentEvent documentEvent) {
			//we check the edit text fields for validity.  This allows violations to be printed in the error section that shows up at the bottom of the popup modal
			checkFieldsForValidation(firstname_field_edit.getText(), lastname_field_edit.getText(), address_line1_field_edit.getText(), address_line2_field_edit.getText(), city_field_edit.getText(), state_field_edit.getText(), zipcode_field_edit.getText(), phone_field_edit.getText(), email_field_edit.getText(), facebook_field_edit.getText(), twitter_field_edit.getText(), instagram_field_edit.getText());
		}

		@Override
		public void changedUpdate(DocumentEvent documentEvent) {

		}
	}

	/*
	This is the event handler that is called when a user clicks on a column header, which calls the sorting functionality
	It gets the sort header name and from that determines which kind of sort should occur from a switch statement,
	performs the sort,sets the current definitive "last sort performed" member variable, It then reloads the contats
	data into the JTable with the data being properly sorted
	 */
	private class ContactsBookHeaderEventHandler extends MouseAdapter{
		@Override
		public void mouseClicked(MouseEvent arg0) {
			Point point = arg0.getPoint();
			//get the column number, which will point us to the column name
			Integer column = table.columnAtPoint(point);
			Sorting sorter = new Sorting();
			java.util.ArrayList<Object[]> contactsBook;
			//if we currently have a search term in the search text field then we get our contacts data from the searched contacts data set
			if(_searchTerm.equals("")){
				contactsBook = book.getContactsBook();
			}
			else{
				contactsBook = book.getSearchedContactsBook();
			}
			//here, based on the column index we obtained we can perform our sort using our custom made sorting algorithms.
			switch(column){
				case 0:
					sorter.sortContacts(contactsBook, SortingType.FIRSTNAME, _firstNameHeaderSortUp);
					sortingType = SortingType.FIRSTNAME;
					_firstNameHeaderSortUp = !_firstNameHeaderSortUp;
					break;
				case 1:
					sorter.sortContacts(contactsBook, SortingType.LASTNAME, _lastNameHeaderSortUp);
					sortingType = SortingType.LASTNAME;
					_lastNameHeaderSortUp = !_lastNameHeaderSortUp;
					break;
				case 2:
					sorter.sortContacts(contactsBook, SortingType.ADDRESSLINE1, _addressLine1HeaderSortUp);
					sortingType = SortingType.ADDRESSLINE1;
					_addressLine1HeaderSortUp = !_addressLine1HeaderSortUp;
					break;
				case 3:
					sorter.sortContacts(contactsBook, SortingType.ADDRESSLINE2, _addressLine2HeaderSortUp);
					sortingType = SortingType.ADDRESSLINE2;
					_addressLine2HeaderSortUp = !_addressLine2HeaderSortUp;
					break;
				case 4:
					sorter.sortContacts(contactsBook, SortingType.CITY, _cityHeaderSortUp);
					sortingType = SortingType.CITY;
					_cityHeaderSortUp = !_cityHeaderSortUp;
					break;
				case 5:
					sorter.sortContacts(contactsBook, SortingType.STATE, _stateHeaderSortUp);
					sortingType = SortingType.STATE;
					_stateHeaderSortUp = !_stateHeaderSortUp;
					break;
				case 6:
					sorter.sortContacts(contactsBook, SortingType.ZIPCODE, _zipcodeHeaderSortUp);
					sortingType = SortingType.ZIPCODE;
					_zipcodeHeaderSortUp = !_zipcodeHeaderSortUp;
					break;
				case 7:
					sorter.sortContacts(contactsBook, SortingType.PHONE, _phoneHeaderSortUp);
					sortingType = SortingType.PHONE;
					_phoneHeaderSortUp = !_phoneHeaderSortUp;
					break;
				case 8:
					sorter.sortContacts(contactsBook, SortingType.EMAIL, _emailHeaderSortUp);
					sortingType = SortingType.EMAIL;
					_emailHeaderSortUp = !_emailHeaderSortUp;
					break;
			}
			//set the model with this new data which has been sorted
			book.setContactsBook(contactsBook);
			//load the JTable with the sorted contacts data
			loadJTableWithData(contactsBook);

		}
	}

	/*
    This is the event handler that is called when a user clicks on a contact from within the JTable.
    It gets that contact from the model and loads the contact view TextPane in the popup modal with
    the contact attributes.
     */
	private class ViewContactTriggerModalEventHandler extends MouseAdapter{
		@Override
		public void mouseClicked(MouseEvent arg0) {
			selectedRow = table.getSelectedRow();
			Object[] lastRowClicked;
			if(selectedRow != -1) {
				//Need to populate the modal fields with data
				if(_searchTerm.equals("")){
					lastRowClicked = book.getContactsBook().get(selectedRow);
				}
				else{
					lastRowClicked = book.getSearchedContactsBook().get(selectedRow);
				}
				String cssFormatting = "font-size:12px; font-weight:bold;'";

				firstname_view_pane.setText(writeHtmlForBody((String) lastRowClicked[ContactAttributeIndexer.FIRSTNAME.getIndex()],cssFormatting));
				lastname_view_pane.setText(writeHtmlForBody((String) lastRowClicked[ContactAttributeIndexer.LASTNAME.getIndex()],cssFormatting));
				address_line1_view_pane.setText(writeHtmlForBody((String) lastRowClicked[ContactAttributeIndexer.STREETADDRESS1.getIndex()],cssFormatting));
				address_line2_view_pane.setText(writeHtmlForBody((String) lastRowClicked[ContactAttributeIndexer.STREETADDRESS2.getIndex()],cssFormatting));
				city_view_pane.setText(writeHtmlForBody((String) lastRowClicked[ContactAttributeIndexer.CITY.getIndex()],cssFormatting));
				state_view_pane.setText(writeHtmlForBody((String) lastRowClicked[ContactAttributeIndexer.STATE.getIndex()],cssFormatting));
				zipcode_view_pane.setText(writeHtmlForBody((String) lastRowClicked[ContactAttributeIndexer.ZIPCODE.getIndex()],cssFormatting));
				phone_view_pane.setText(writeHtmlForBody((String) lastRowClicked[ContactAttributeIndexer.PHONENUMBER.getIndex()],cssFormatting));
				email_view_pane.setText(writeHtmlForBody((String) lastRowClicked[ContactAttributeIndexer.EMAIL.getIndex()],cssFormatting));

				String facebookUrlValue = (String) lastRowClicked[ContactAttributeIndexer.FACEBOOK.getIndex()];
				String instagramUrlValue = (String) lastRowClicked[ContactAttributeIndexer.INSTAGRAM.getIndex()];
				String twitterUrlValue = (String) lastRowClicked[ContactAttributeIndexer.TWITTER.getIndex()];
				twitter_view_pane.setVisible(true);
				if(!facebookUrlValue.equals("")){
					facebook_view_pane.setVisible(true);
				}
				else{
					facebook_view_pane.setVisible(false);
				}
				if(!twitterUrlValue.equals("")){
					twitter_view_pane.setVisible(true);
				}
				else{
					twitter_view_pane.setVisible(false);
				}
				if(!instagramUrlValue.equals("")){
					instagram_view_pane.setVisible(true);
				}
				else{
					instagram_view_pane.setVisible(false);
				}



				//Resize height of panes
				for(JTextPane pane : panes){
					pane.setSize(pane.getWidth(), pane.getPreferredSize().height);
				}

				//Disable main window
				frame.setEnabled(false);
				//Show add options for modal
				popup.setTitle("Viewing Contact");
				cards.show(panel_container, VIEW_PANEL);
				//show modal
				popup.setVisible(true);
			}
		}
	}
	/*
	This is the event handler that is called when the user clicks the "Add Contact" button.
	It calls the logic which opens up a popup modal with the contact's attributes with options
	to edit or delete
	 */
	private class AddContactTriggerModalEventHandler extends MouseAdapter{
		@Override
		public void mouseClicked(MouseEvent arg0) {
			addContactTriggerModal();
		}
	}
	/*
	This is the event handler that is called when the user clicks on the "Edit Contact" button
	from within the contact view popup modal.  It loads all of the contact attributes from the model
	to the contact attributes from within the contact edit popup modal from which a user can
	then edit and save from.
	*/
	private class EditContactTriggerModalEventHandler implements ActionListener{

		@Override
		//The following code is run when a user clicks "Edit" while viewing a contact
		public void actionPerformed(ActionEvent e){

			int selectedRow = table.getSelectedRow();
			if(selectedRow != -1) {
				//First get the contact in question from the model
				Object[] lastRowClicked;
				if(_searchTerm.equals("")){
					lastRowClicked = book.getContactsBook().get(selectedRow);
				}
				else{
					lastRowClicked = book.getSearchedContactsBook().get(selectedRow);
				}
				//load the edit contact attributes fields from which a user can edit their values
				firstname_field_edit.setText((String) lastRowClicked[ContactAttributeIndexer.FIRSTNAME.getIndex()]);
				lastname_field_edit.setText((String) lastRowClicked[ContactAttributeIndexer.LASTNAME.getIndex()]);
				address_line1_field_edit.setText((String) lastRowClicked[ContactAttributeIndexer.STREETADDRESS1.getIndex()]);
				address_line2_field_edit.setText((String) lastRowClicked[ContactAttributeIndexer.STREETADDRESS2.getIndex()]);
				city_field_edit.setText((String) lastRowClicked[ContactAttributeIndexer.CITY.getIndex()]);
				state_field_edit.setText((String) lastRowClicked[ContactAttributeIndexer.STATE.getIndex()]);
				zipcode_field_edit.setText((String) lastRowClicked[ContactAttributeIndexer.ZIPCODE.getIndex()]);
				phone_field_edit.setText((String) lastRowClicked[ContactAttributeIndexer.PHONENUMBER.getIndex()]);
				email_field_edit.setText((String) lastRowClicked[ContactAttributeIndexer.EMAIL.getIndex()]);
				facebook_field_edit.setText((String) lastRowClicked[ContactAttributeIndexer.FACEBOOK.getIndex()]);
				instagram_field_edit.setText((String) lastRowClicked[ContactAttributeIndexer.INSTAGRAM.getIndex()]);
				twitter_field_edit.setText((String) lastRowClicked[ContactAttributeIndexer.TWITTER.getIndex()]);
				//make the edit panel visible
				cards.show(panel_container, EDIT_PANEL);
			}
		}
	}
	/*
	This is the event handler that is called when the user is in the create contact popup modal and instead of progressing forward and saving their contact
	data they click the "cancel" button which causes the modal to close and the create contact operation to terminate
	 */
	private class CancelCreateContactEventHandler implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			//If cancel is clicked while editing, just show view panel again.
			cards.show(panel_container, VIEW_PANEL);
		}
	}
	/*
	This is the event handler that is called when a user has the create contact popup modal appear and they fill out the new contact information.
	After they click the "create" button this event is called.  First we validate the data and we then create a contact attribute array and add
	that to the "book" member variable.  We then sort the contacts in book array and load those contacts into the JTable.
	 */
	private class CreateContactEventHandler implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent actionEvent) {

			//first we validate the fields
			if(!checkFieldsForValidation(firstname_field.getText(), lastname_field.getText(), address_line1_field.getText(), address_line2_field.getText(), city_field.getText(), state_field.getText(), zipcode_field.getText(), phone_field.getText(), email_field.getText(), facebook_field.getText(), twitter_field.getText(), instagram_field.getText())){
				return;
			}
			//create a contact attribute array from the add contact fields the user saved from
			Object[] form_data = {firstname_field.getText(),
					lastname_field.getText(),
					address_line1_field.getText(),
					address_line2_field.getText(),
					city_field.getText(),
					state_field.getText(),
					zipcode_field.getText(),
					phone_field.getText(),
					email_field.getText(),
					facebook_field.getText(),
					twitter_field.getText(),
					instagram_field.getText(),

			};
			altered = true;
			//add the new contacts attribute array to our book's model that holds our Contacts
			book.getContactsBook().add(form_data);
			Sorting sorting = new Sorting();
			//sort the contacts within "book" member variable
			sorting.sortContacts(book.getContactsBook(), sortingType,  getSortForSortingTypeForEditCreateContact(sortingType));
			//load the sorted contacts into the JTable
			loadJTableWithData(book.getContactsBook());

			clearTextFields();       //Clear form of data
			frame.setEnabled(true);  //Re-enable main window
			popup.dispose();		 //Close current popup

		}
	}
	/*
	This method is called after the user is in contact view mode in the popup modal, and from there the user clicks the Delete Contact
	button.  The method gets the currently selected row number which is the row to be deleted, then a popup confirm dialog is created,
	and if confirmed the corresponding contact is removed from the "book" member variable, and the table is loaded with the updated
	model data which is now missing the contact we just deleted
	 */
	private class DeleteContactEventHandler extends MouseAdapter{
		@Override
		public void mouseClicked(MouseEvent arg0) {
			int rowToDelete = table.getSelectedRow();

			//Make sure a row is selected before trying to delete, and confirm with user
			if(rowToDelete != -1 && renderConfirmationModal("Delete Contact", "Delete contact? (This action cannot be undone)")) {
				//then we are going to delete it from the model and reload the jtable with the model
				//load the JTable with the updated model data
				if(_searchTerm.equals("")){
					book.getContactsBook().remove(rowToDelete);
					loadJTableWithData(book.getContactsBook());

				}
				else{
					//the user is deleting from a searched item. the index to be deleted is not the same index as for the non sorted data structure, so we have to do a search match delete on non sorted data structure
					Object[] deletedContact = book.getSearchedContactsBook().get(rowToDelete);
					deleteContactThatMatches(deletedContact);
					book.getSearchedContactsBook().remove(rowToDelete);
					loadJTableWithData(book.getSearchedContactsBook());
				}
				altered = true;
				frame.setEnabled(true);
				popup.dispose();
			}
		}
		/*
		This method is responsible for deleting contacts from contactsBook that match a deleted contact from the sorted contact data set
 		*/
		private void deleteContactThatMatches(Object[] deletedContact) {


			for(int i=0; i < book.getContactsBook().size(); i++){
				Object[] current = book.getContactsBook().get(i);
				String firstName = (String) current[ContactAttributeIndexer.FIRSTNAME.getIndex()];
				String lastName = (String) current[ContactAttributeIndexer.LASTNAME.getIndex()];
				String addressLine1 = (String) current[ContactAttributeIndexer.STREETADDRESS1.getIndex()];
				String addressLine2 = (String) current[ContactAttributeIndexer.STREETADDRESS2.getIndex()];
				String city = (String) current[ContactAttributeIndexer.CITY.getIndex()];
				String state = (String) current[ContactAttributeIndexer.STATE.getIndex()];
				String zipcode = (String) current[ContactAttributeIndexer.ZIPCODE.getIndex()];
				String phoneNumber = (String) current[ContactAttributeIndexer.PHONENUMBER.getIndex()];
				String email = (String) current[ContactAttributeIndexer.EMAIL.getIndex()];
				String facebook = (String) current[ContactAttributeIndexer.FACEBOOK.getIndex()];
				String instagram = (String) current[ContactAttributeIndexer.INSTAGRAM.getIndex()];
				String twitter = (String) current[ContactAttributeIndexer.TWITTER.getIndex()];

				String firstNameDeleted = (String) deletedContact[ContactAttributeIndexer.FIRSTNAME.getIndex()];
				String lastNameDeleted  = (String) deletedContact[ContactAttributeIndexer.LASTNAME.getIndex()];
				String addressLine1Deleted  = (String) deletedContact[ContactAttributeIndexer.STREETADDRESS1.getIndex()];
				String addressLine2Deleted  = (String) current[ContactAttributeIndexer.STREETADDRESS2.getIndex()];
				String cityDeleted  = (String) deletedContact[ContactAttributeIndexer.CITY.getIndex()];
				String stateDeleted  = (String) deletedContact[ContactAttributeIndexer.STATE.getIndex()];
				String zipcodeDeleted  = (String) deletedContact[ContactAttributeIndexer.ZIPCODE.getIndex()];
				String phoneNumberDeleted  = (String) deletedContact[ContactAttributeIndexer.PHONENUMBER.getIndex()];
				String emailDeleted  = (String) deletedContact[ContactAttributeIndexer.EMAIL.getIndex()];
				String facebookDeleted  = (String) deletedContact[ContactAttributeIndexer.FACEBOOK.getIndex()];
				String instagramDeleted  = (String) deletedContact[ContactAttributeIndexer.INSTAGRAM.getIndex()];
				String twitterDeleted  = (String) deletedContact[ContactAttributeIndexer.TWITTER.getIndex()];

				if(!firstName.equals(firstNameDeleted)){
					continue;
				}
				if(!lastName.equals(lastNameDeleted)){
					continue;
				}
				if(!addressLine1.equals(addressLine1Deleted)){
					continue;
				}
				if(!addressLine2.equals(addressLine2Deleted)){
					continue;
				}
				if(!city.equals(cityDeleted)){
					continue;
				}
				if(!state.equals(stateDeleted)){
					continue;
				}
				if(!zipcode.equals(zipcodeDeleted)){
					continue;
				}
				if(!phoneNumber.equals(phoneNumberDeleted)){
					continue;
				}
				if(!email.equals(emailDeleted)){
					continue;
				}
				if(!facebook.equals(facebookDeleted)){
					continue;
				}
				if(!instagram.equals(instagramDeleted)){
					continue;
				}
				if(!twitter.equals(twitterDeleted)){
					continue;
				}
				//if we get here then all of the contact attributes match so we can delete this
				book.getContactsBook().remove(i);

			}
		}
	}
	/*
	This method is called after the user is in User Edit mode and saves their edit.
	The current value for this contact from the member variable "books", and overwrites
	the contact's current attributes with the attributes from the saved edit.
	The JTable is then reloaded with the updated books member variable data
	*/
	private class SaveContactEventHandler implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
            /*
                Inside the modal, when edit button is clicked
                Send text field data to backend to be validated
            */
			if(!checkFieldsForValidation(firstname_field_edit.getText(), lastname_field_edit.getText(),address_line1_field_edit.getText(), address_line2_field_edit.getText(), city_field_edit.getText(), state_field_edit.getText(), zipcode_field_edit.getText(), phone_field_edit.getText(), email_field_edit.getText(), facebook_field_edit.getText(), twitter_field_edit.getText(), instagram_field_edit.getText())){
				return;
			}
			altered = true;
			//Here we get the contact's attribute array from the "books" member variable and over write the data with the data from the contact edit save
			Object[] selectedRowFromModel;
			if(_searchTerm.equals("")){
				selectedRowFromModel = book.getContactsBook().get(selectedRow);
			}
			else{
				selectedRowFromModel = book.getSearchedContactsBook().get(selectedRow);
			}
			selectedRowFromModel[ContactAttributeIndexer.FIRSTNAME.getIndex()] = firstname_field_edit.getText();
			selectedRowFromModel[ContactAttributeIndexer.LASTNAME.getIndex()] = lastname_field_edit.getText();
			selectedRowFromModel[ContactAttributeIndexer.STREETADDRESS1.getIndex()] = address_line1_field_edit.getText();
			selectedRowFromModel[ContactAttributeIndexer.STREETADDRESS2.getIndex()] = address_line2_field_edit.getText();
			selectedRowFromModel[ContactAttributeIndexer.CITY.getIndex()] = city_field_edit.getText();
			selectedRowFromModel[ContactAttributeIndexer.STATE.getIndex()] = state_field_edit.getText();
			selectedRowFromModel[ContactAttributeIndexer.ZIPCODE.getIndex()] = zipcode_field_edit.getText();
			selectedRowFromModel[ContactAttributeIndexer.PHONENUMBER.getIndex()] = phone_field_edit.getText();
			selectedRowFromModel[ContactAttributeIndexer.EMAIL.getIndex()] = email_field_edit.getText();
			selectedRowFromModel[ContactAttributeIndexer.FACEBOOK.getIndex()] = facebook_field_edit.getText();
			selectedRowFromModel[ContactAttributeIndexer.INSTAGRAM.getIndex()] = instagram_field_edit.getText();
			selectedRowFromModel[ContactAttributeIndexer.TWITTER.getIndex()] = twitter_field_edit.getText();


			//Here we sort by the current sorting type and the ordering for that sorting type, either ascending or decending
			Sorting sorting = new Sorting();
			sorting.sortContacts(book.getContactsBook(), sortingType,  getSortForSortingTypeForEditCreateContact(sortingType));

			//load the JTable with the updated model data
			if(_searchTerm.equals("")){
				loadJTableWithData(book.getContactsBook());
			}
			else{
				loadJTableWithData(book.getSearchedContactsBook());
			}


			frame.setEnabled(true);
			//we close the popup because we are done with our edit save
			popup.dispose();
			//clear the JtextPane text for the JtextPanes in the modals.
			clearTextFields();
		}
	}


}
