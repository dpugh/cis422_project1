package wusers;

/*
This is an enum that takes a particular contact attribute and tells you what the index should
be if you want the data you create from it to fit into what JTable is expecting
 */
public enum ContactAttributeIndexer {

    FIRSTNAME (0),
    LASTNAME(1),
    STREETADDRESS1(2),
    STREETADDRESS2(3),
    CITY(4),
    STATE(5),
    ZIPCODE(6),
    PHONENUMBER(7),
    EMAIL(8),
    FACEBOOK(9),
    TWITTER(10),
    INSTAGRAM(11);

    private Integer _index = 0;

    ContactAttributeIndexer(Integer index){
        _index = index;
    }
    Integer getIndex(){
        return _index;
    }

}
